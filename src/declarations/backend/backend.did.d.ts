import type { Principal } from '@dfinity/principal';
import type { ActorMethod } from '@dfinity/agent';

export interface Board {
  'id' : string,
  'postCount' : number,
  'bumpTimestamp' : bigint,
  'views' : number,
  'ownerPrincipal' : string,
  'gate' : Gate,
  'name' : string,
  'fileCount' : number,
  'properties' : BoardProperties,
  'fileSize' : number,
  'threadCount' : number,
  'timestamp' : bigint,
}
export interface BoardCreate {
  'id' : string,
  'gate' : Gate,
  'name' : string,
  'properties' : BoardProperties,
}
export interface BoardProperties {
  'flags' : boolean,
  'unListed' : boolean,
  'text' : boolean,
  'anonymous' : boolean,
}
export type CreateError = { 'AlreadyExists' : null };
export type CreateMessageError = { 'UserNotFound' : null };
export type CreatePostError = { 'BoardNotFound' : null };
export type Error = { 'AlreadyVoted' : null } |
  { 'Error' : null } |
  { 'AlreadyReported' : null } |
  { 'NotFound' : null } |
  { 'NotAuthorized' : null } |
  { 'AlreadyExists' : null };
export interface Gate { 'nft' : string, 'token' : string, 'amount' : number }
export interface IPAddress { 'ipAddress' : string, 'postId' : string }
export interface Message {
  'subject' : string,
  'body' : string,
  'timestamp' : bigint,
}
export interface MessageCreate {
  'subject' : string,
  'body' : string,
  'toPrincipal' : Principal,
  'toPosterId' : PosterId,
  'fromPosterId' : PosterId,
  'postId' : string,
}
export interface Poll {
  'participants' : string,
  'question' : string,
  'choices' : Array<PollChoice>,
}
export interface PollChoice { 'voters' : Array<string>, 'choice' : string }
export interface Post {
  'id' : string,
  'bumpTimestamp' : bigint,
  'subject' : string,
  'body' : string,
  'file' : PostFile,
  'gate' : Gate,
  'poll' : Poll,
  'web3' : Web3,
  'threadDetails' : ThreadDetails,
  'properties' : Properties,
  'boardId' : string,
  'timestamp' : bigint,
  'reports' : Reports,
  'threadId' : string,
  'poster' : Poster,
}
export interface PostCreate {
  'id' : string,
  'subject' : string,
  'body' : string,
  'file' : PostFile,
  'gate' : Gate,
  'poll' : Poll,
  'web3' : Web3In,
  'properties' : Properties,
  'boardId' : string,
  'threadId' : string,
  'poster' : PosterIn,
}
export interface PostFile {
  'name' : string,
  'nsfw' : boolean,
  'path' : string,
  'size' : number,
  'format' : string,
}
export interface Poster {
  'posterId' : PosterId,
  'country' : string,
  'flag' : string,
  'badges' : Array<string>,
  'directMessage' : boolean,
}
export type PosterId = { 'Anon' : string } |
  { 'Admin' : string } |
  { 'Principal' : string } |
  { 'Username' : string };
export interface PosterIn {
  'posterId' : PosterId,
  'country' : string,
  'flag' : string,
  'badges' : Array<string>,
  'directMessage' : boolean,
  'ipAddress' : string,
}
export interface Properties {
  'op' : boolean,
  'locked' : boolean,
  'saged' : boolean,
  'stickied' : boolean,
  'flagged' : boolean,
}
export interface Reports {
  'seaChadPrincipals' : Array<Principal>,
  'count' : number,
}
export type Result = { 'ok' : User } |
  { 'err' : Error };
export type Result_1 = { 'ok' : Post } |
  { 'err' : Error };
export type Result_10 = { 'ok' : Message } |
  { 'err' : CreateMessageError };
export type Result_11 = { 'ok' : Board } |
  { 'err' : CreateError };
export type Result_2 = { 'ok' : Array<User> } |
  { 'err' : Error };
export type Result_3 = { 'ok' : Array<Post> } |
  { 'err' : Error };
export type Result_4 = { 'ok' : Array<Board> } |
  { 'err' : Error };
export type Result_5 = { 'ok' : Array<IPAddress> } |
  { 'err' : Error };
export type Result_6 = { 'ok' : number } |
  { 'err' : Error };
export type Result_7 = { 'ok' : string } |
  { 'err' : Error };
export type Result_8 = { 'ok' : Board } |
  { 'err' : Error };
export type Result_9 = { 'ok' : Post } |
  { 'err' : CreatePostError };
export interface ThreadDetails {
  'postIds' : Array<string>,
  'views' : number,
  'fileCount' : number,
  'fileSize' : number,
  'uniquePosters' : Array<PosterId>,
}
export interface User {
  'principal' : Principal,
  'navbarBoards' : Array<string>,
  'badges' : Array<string>,
  'savedPosts' : {
    'watchedThreads' : Array<string>,
    'hiddenPosts' : Array<string>,
    'minimizedPosts' : Array<string>,
  },
  'preferences' : UserPreferences,
  'timestamp' : bigint,
  'usernames' : Array<string>,
  'principalProvider' : string,
}
export interface UserPreferences {
  'threadView' : string,
  'showNsfw' : boolean,
  'boardView' : string,
  'showFlagged' : boolean,
  'directMessaging' : boolean,
}
export interface Web3 {
  'icpReceived' : number,
  'acceptIcp' : boolean,
  'icpBalance' : number,
}
export interface Web3In { 'acceptIcp' : boolean, 'icpBalance' : number }
export interface _SERVICE {
  'acceptCycles' : ActorMethod<[], undefined>,
  'addBadge' : ActorMethod<[string], Result>,
  'addNavbarBoard' : ActorMethod<[string], Result>,
  'addPostToBoard' : ActorMethod<[string, Post], undefined>,
  'addPostToThread' : ActorMethod<[string, Post], undefined>,
  'banIP' : ActorMethod<[string], Result_7>,
  'boardExists' : ActorMethod<[string], boolean>,
  'claimUsername' : ActorMethod<[string], Result>,
  'clearBannedIps' : ActorMethod<[], undefined>,
  'clearMessages' : ActorMethod<[], undefined>,
  'clearWatchedThreads' : ActorMethod<[], Result>,
  'createBoard' : ActorMethod<[BoardCreate], Result_11>,
  'createMessage' : ActorMethod<[MessageCreate], Result_10>,
  'createPost' : ActorMethod<[PostCreate], Result_9>,
  'deleteAllBoards' : ActorMethod<[], Result_7>,
  'deleteAllPosts' : ActorMethod<[], Result_7>,
  'deleteAllUsers' : ActorMethod<[], Result_7>,
  'deleteBoard' : ActorMethod<[string], Result_7>,
  'deleteNavbarBoard' : ActorMethod<[string], Result>,
  'deleteUsername' : ActorMethod<[string], Result>,
  'getBackendCanisterId' : ActorMethod<[], Principal>,
  'getBackendCycleBalance' : ActorMethod<[], bigint>,
  'getBannedIps' : ActorMethod<[], Array<string>>,
  'getBoard' : ActorMethod<[string], Result_8>,
  'getBoardCount' : ActorMethod<[], bigint>,
  'getBoardFileUploadSize' : ActorMethod<[string], number>,
  'getBoardPostCount' : ActorMethod<[string], bigint>,
  'getBoardThreadCount' : ActorMethod<[string], bigint>,
  'getBoardThreads' : ActorMethod<[string], Result_3>,
  'getBoards' : ActorMethod<[], Result_4>,
  'getFileCount' : ActorMethod<[], bigint>,
  'getFileUploadSize' : ActorMethod<[], number>,
  'getFrontendCanisterId' : ActorMethod<[], Principal>,
  'getFrontendCycleBalance' : ActorMethod<[], bigint>,
  'getIPAddresses' : ActorMethod<[], Result_5>,
  'getIpAddress' : ActorMethod<[string], Result_7>,
  'getListedBoards' : ActorMethod<[], Result_4>,
  'getNextPostId' : ActorMethod<[string], Result_6>,
  'getOrCreateUser' : ActorMethod<[string], Result>,
  'getPost' : ActorMethod<[string], Result_1>,
  'getPostCount' : ActorMethod<[], bigint>,
  'getPosts' : ActorMethod<[], Result_3>,
  'getPostsFromIds' : ActorMethod<[Array<string>], Result_3>,
  'getPrincipal' : ActorMethod<[], string>,
  'getThread' : ActorMethod<[string, string], Result_3>,
  'getThreadCount' : ActorMethod<[], bigint>,
  'getThreads' : ActorMethod<[], Result_3>,
  'getUser' : ActorMethod<[Principal], Result>,
  'getUserCount' : ActorMethod<[], bigint>,
  'getUsers' : ActorMethod<[], Result_2>,
  'getVisitCount' : ActorMethod<[], number>,
  'importBoards' : ActorMethod<[Array<Board>], Result_4>,
  'importIPs' : ActorMethod<[Array<IPAddress>], Result_5>,
  'importNewBoards' : ActorMethod<[Array<Board>], Result_4>,
  'importNewPosts' : ActorMethod<[Array<PostCreate>], Result_3>,
  'importPost' : ActorMethod<[Post], Result_1>,
  'importPosts' : ActorMethod<[Array<Post>], Result_3>,
  'importUsers' : ActorMethod<[Array<User>], Result_2>,
  'incrementBoardViewCount' : ActorMethod<[string], undefined>,
  'incrementThreadViewCount' : ActorMethod<[string], undefined>,
  'incrementVisitCount' : ActorMethod<[], undefined>,
  'removePost' : ActorMethod<[string], Result_1>,
  'reportPost' : ActorMethod<[string, bigint], Result_1>,
  'setBoardViewPreference' : ActorMethod<[string], Result>,
  'setThreadViewPreference' : ActorMethod<[string], Result>,
  'submitPollResponse' : ActorMethod<[string, PollChoice], Result_1>,
  'toggleHiddenPost' : ActorMethod<[string], Result>,
  'toggleMinimizedPost' : ActorMethod<[string], Result>,
  'toggleNSFW' : ActorMethod<[string], Result_1>,
  'toggleShowFlagged' : ActorMethod<[], Result>,
  'toggleShowNsfw' : ActorMethod<[], Result>,
  'toggleWatchedThread' : ActorMethod<[string], Result>,
}

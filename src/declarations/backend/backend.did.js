export const idlFactory = ({ IDL }) => {
  const UserPreferences = IDL.Record({
    'threadView' : IDL.Text,
    'showNsfw' : IDL.Bool,
    'boardView' : IDL.Text,
    'showFlagged' : IDL.Bool,
    'directMessaging' : IDL.Bool,
  });
  const User = IDL.Record({
    'principal' : IDL.Principal,
    'navbarBoards' : IDL.Vec(IDL.Text),
    'badges' : IDL.Vec(IDL.Text),
    'savedPosts' : IDL.Record({
      'watchedThreads' : IDL.Vec(IDL.Text),
      'hiddenPosts' : IDL.Vec(IDL.Text),
      'minimizedPosts' : IDL.Vec(IDL.Text),
    }),
    'preferences' : UserPreferences,
    'timestamp' : IDL.Int,
    'usernames' : IDL.Vec(IDL.Text),
    'principalProvider' : IDL.Text,
  });
  const Error = IDL.Variant({
    'AlreadyVoted' : IDL.Null,
    'Error' : IDL.Null,
    'AlreadyReported' : IDL.Null,
    'NotFound' : IDL.Null,
    'NotAuthorized' : IDL.Null,
    'AlreadyExists' : IDL.Null,
  });
  const Result = IDL.Variant({ 'ok' : User, 'err' : Error });
  const PostFile = IDL.Record({
    'name' : IDL.Text,
    'nsfw' : IDL.Bool,
    'path' : IDL.Text,
    'size' : IDL.Nat32,
    'format' : IDL.Text,
  });
  const Gate = IDL.Record({
    'nft' : IDL.Text,
    'token' : IDL.Text,
    'amount' : IDL.Nat32,
  });
  const PollChoice = IDL.Record({
    'voters' : IDL.Vec(IDL.Text),
    'choice' : IDL.Text,
  });
  const Poll = IDL.Record({
    'participants' : IDL.Text,
    'question' : IDL.Text,
    'choices' : IDL.Vec(PollChoice),
  });
  const Web3 = IDL.Record({
    'icpReceived' : IDL.Float64,
    'acceptIcp' : IDL.Bool,
    'icpBalance' : IDL.Float64,
  });
  const PosterId = IDL.Variant({
    'Anon' : IDL.Text,
    'Admin' : IDL.Text,
    'Principal' : IDL.Text,
    'Username' : IDL.Text,
  });
  const ThreadDetails = IDL.Record({
    'postIds' : IDL.Vec(IDL.Text),
    'views' : IDL.Nat32,
    'fileCount' : IDL.Nat32,
    'fileSize' : IDL.Nat32,
    'uniquePosters' : IDL.Vec(PosterId),
  });
  const Properties = IDL.Record({
    'op' : IDL.Bool,
    'locked' : IDL.Bool,
    'saged' : IDL.Bool,
    'stickied' : IDL.Bool,
    'flagged' : IDL.Bool,
  });
  const Reports = IDL.Record({
    'seaChadPrincipals' : IDL.Vec(IDL.Principal),
    'count' : IDL.Nat32,
  });
  const Poster = IDL.Record({
    'posterId' : PosterId,
    'country' : IDL.Text,
    'flag' : IDL.Text,
    'badges' : IDL.Vec(IDL.Text),
    'directMessage' : IDL.Bool,
  });
  const Post = IDL.Record({
    'id' : IDL.Text,
    'bumpTimestamp' : IDL.Int,
    'subject' : IDL.Text,
    'body' : IDL.Text,
    'file' : PostFile,
    'gate' : Gate,
    'poll' : Poll,
    'web3' : Web3,
    'threadDetails' : ThreadDetails,
    'properties' : Properties,
    'boardId' : IDL.Text,
    'timestamp' : IDL.Int,
    'reports' : Reports,
    'threadId' : IDL.Text,
    'poster' : Poster,
  });
  const Result_7 = IDL.Variant({ 'ok' : IDL.Text, 'err' : Error });
  const BoardProperties = IDL.Record({
    'flags' : IDL.Bool,
    'unListed' : IDL.Bool,
    'text' : IDL.Bool,
    'anonymous' : IDL.Bool,
  });
  const BoardCreate = IDL.Record({
    'id' : IDL.Text,
    'gate' : Gate,
    'name' : IDL.Text,
    'properties' : BoardProperties,
  });
  const Board = IDL.Record({
    'id' : IDL.Text,
    'postCount' : IDL.Nat32,
    'bumpTimestamp' : IDL.Int,
    'views' : IDL.Nat32,
    'ownerPrincipal' : IDL.Text,
    'gate' : Gate,
    'name' : IDL.Text,
    'fileCount' : IDL.Nat32,
    'properties' : BoardProperties,
    'fileSize' : IDL.Nat32,
    'threadCount' : IDL.Nat32,
    'timestamp' : IDL.Int,
  });
  const CreateError = IDL.Variant({ 'AlreadyExists' : IDL.Null });
  const Result_11 = IDL.Variant({ 'ok' : Board, 'err' : CreateError });
  const MessageCreate = IDL.Record({
    'subject' : IDL.Text,
    'body' : IDL.Text,
    'toPrincipal' : IDL.Principal,
    'toPosterId' : PosterId,
    'fromPosterId' : PosterId,
    'postId' : IDL.Text,
  });
  const Message = IDL.Record({
    'subject' : IDL.Text,
    'body' : IDL.Text,
    'timestamp' : IDL.Int,
  });
  const CreateMessageError = IDL.Variant({ 'UserNotFound' : IDL.Null });
  const Result_10 = IDL.Variant({ 'ok' : Message, 'err' : CreateMessageError });
  const Web3In = IDL.Record({
    'acceptIcp' : IDL.Bool,
    'icpBalance' : IDL.Float64,
  });
  const PosterIn = IDL.Record({
    'posterId' : PosterId,
    'country' : IDL.Text,
    'flag' : IDL.Text,
    'badges' : IDL.Vec(IDL.Text),
    'directMessage' : IDL.Bool,
    'ipAddress' : IDL.Text,
  });
  const PostCreate = IDL.Record({
    'id' : IDL.Text,
    'subject' : IDL.Text,
    'body' : IDL.Text,
    'file' : PostFile,
    'gate' : Gate,
    'poll' : Poll,
    'web3' : Web3In,
    'properties' : Properties,
    'boardId' : IDL.Text,
    'threadId' : IDL.Text,
    'poster' : PosterIn,
  });
  const CreatePostError = IDL.Variant({ 'BoardNotFound' : IDL.Null });
  const Result_9 = IDL.Variant({ 'ok' : Post, 'err' : CreatePostError });
  const Result_8 = IDL.Variant({ 'ok' : Board, 'err' : Error });
  const Result_3 = IDL.Variant({ 'ok' : IDL.Vec(Post), 'err' : Error });
  const Result_4 = IDL.Variant({ 'ok' : IDL.Vec(Board), 'err' : Error });
  const IPAddress = IDL.Record({ 'ipAddress' : IDL.Text, 'postId' : IDL.Text });
  const Result_5 = IDL.Variant({ 'ok' : IDL.Vec(IPAddress), 'err' : Error });
  const Result_6 = IDL.Variant({ 'ok' : IDL.Nat32, 'err' : Error });
  const Result_1 = IDL.Variant({ 'ok' : Post, 'err' : Error });
  const Result_2 = IDL.Variant({ 'ok' : IDL.Vec(User), 'err' : Error });
  return IDL.Service({
    'acceptCycles' : IDL.Func([], [], []),
    'addBadge' : IDL.Func([IDL.Text], [Result], []),
    'addNavbarBoard' : IDL.Func([IDL.Text], [Result], []),
    'addPostToBoard' : IDL.Func([IDL.Text, Post], [], ['oneway']),
    'addPostToThread' : IDL.Func([IDL.Text, Post], [], ['oneway']),
    'banIP' : IDL.Func([IDL.Text], [Result_7], []),
    'boardExists' : IDL.Func([IDL.Text], [IDL.Bool], ['query']),
    'claimUsername' : IDL.Func([IDL.Text], [Result], []),
    'clearBannedIps' : IDL.Func([], [], ['oneway']),
    'clearMessages' : IDL.Func([], [], ['oneway']),
    'clearWatchedThreads' : IDL.Func([], [Result], []),
    'createBoard' : IDL.Func([BoardCreate], [Result_11], []),
    'createMessage' : IDL.Func([MessageCreate], [Result_10], []),
    'createPost' : IDL.Func([PostCreate], [Result_9], []),
    'deleteAllBoards' : IDL.Func([], [Result_7], []),
    'deleteAllPosts' : IDL.Func([], [Result_7], []),
    'deleteAllUsers' : IDL.Func([], [Result_7], []),
    'deleteBoard' : IDL.Func([IDL.Text], [Result_7], []),
    'deleteNavbarBoard' : IDL.Func([IDL.Text], [Result], []),
    'deleteUsername' : IDL.Func([IDL.Text], [Result], []),
    'getBackendCanisterId' : IDL.Func([], [IDL.Principal], []),
    'getBackendCycleBalance' : IDL.Func([], [IDL.Nat], ['query']),
    'getBannedIps' : IDL.Func([], [IDL.Vec(IDL.Text)], ['query']),
    'getBoard' : IDL.Func([IDL.Text], [Result_8], ['query']),
    'getBoardCount' : IDL.Func([], [IDL.Nat], ['query']),
    'getBoardFileUploadSize' : IDL.Func([IDL.Text], [IDL.Nat32], ['query']),
    'getBoardPostCount' : IDL.Func([IDL.Text], [IDL.Nat], ['query']),
    'getBoardThreadCount' : IDL.Func([IDL.Text], [IDL.Nat], ['query']),
    'getBoardThreads' : IDL.Func([IDL.Text], [Result_3], ['query']),
    'getBoards' : IDL.Func([], [Result_4], ['query']),
    'getFileCount' : IDL.Func([], [IDL.Nat], ['query']),
    'getFileUploadSize' : IDL.Func([], [IDL.Nat32], ['query']),
    'getFrontendCanisterId' : IDL.Func([], [IDL.Principal], []),
    'getFrontendCycleBalance' : IDL.Func([], [IDL.Nat], []),
    'getIPAddresses' : IDL.Func([], [Result_5], []),
    'getIpAddress' : IDL.Func([IDL.Text], [Result_7], []),
    'getListedBoards' : IDL.Func([], [Result_4], ['query']),
    'getNextPostId' : IDL.Func([IDL.Text], [Result_6], ['query']),
    'getOrCreateUser' : IDL.Func([IDL.Text], [Result], []),
    'getPost' : IDL.Func([IDL.Text], [Result_1], ['query']),
    'getPostCount' : IDL.Func([], [IDL.Nat], ['query']),
    'getPosts' : IDL.Func([], [Result_3], ['query']),
    'getPostsFromIds' : IDL.Func([IDL.Vec(IDL.Text)], [Result_3], ['query']),
    'getPrincipal' : IDL.Func([], [IDL.Text], ['query']),
    'getThread' : IDL.Func([IDL.Text, IDL.Text], [Result_3], ['query']),
    'getThreadCount' : IDL.Func([], [IDL.Nat], ['query']),
    'getThreads' : IDL.Func([], [Result_3], ['query']),
    'getUser' : IDL.Func([IDL.Principal], [Result], []),
    'getUserCount' : IDL.Func([], [IDL.Nat], ['query']),
    'getUsers' : IDL.Func([], [Result_2], ['query']),
    'getVisitCount' : IDL.Func([], [IDL.Nat32], ['query']),
    'importBoards' : IDL.Func([IDL.Vec(Board)], [Result_4], []),
    'importIPs' : IDL.Func([IDL.Vec(IPAddress)], [Result_5], []),
    'importNewBoards' : IDL.Func([IDL.Vec(Board)], [Result_4], []),
    'importNewPosts' : IDL.Func([IDL.Vec(PostCreate)], [Result_3], []),
    'importPost' : IDL.Func([Post], [Result_1], []),
    'importPosts' : IDL.Func([IDL.Vec(Post)], [Result_3], []),
    'importUsers' : IDL.Func([IDL.Vec(User)], [Result_2], []),
    'incrementBoardViewCount' : IDL.Func([IDL.Text], [], ['oneway']),
    'incrementThreadViewCount' : IDL.Func([IDL.Text], [], ['oneway']),
    'incrementVisitCount' : IDL.Func([], [], ['oneway']),
    'removePost' : IDL.Func([IDL.Text], [Result_1], []),
    'reportPost' : IDL.Func([IDL.Text, IDL.Nat], [Result_1], []),
    'setBoardViewPreference' : IDL.Func([IDL.Text], [Result], []),
    'setThreadViewPreference' : IDL.Func([IDL.Text], [Result], []),
    'submitPollResponse' : IDL.Func([IDL.Text, PollChoice], [Result_1], []),
    'toggleHiddenPost' : IDL.Func([IDL.Text], [Result], []),
    'toggleMinimizedPost' : IDL.Func([IDL.Text], [Result], []),
    'toggleNSFW' : IDL.Func([IDL.Text], [Result_1], []),
    'toggleShowFlagged' : IDL.Func([], [Result], []),
    'toggleShowNsfw' : IDL.Func([], [Result], []),
    'toggleWatchedThread' : IDL.Func([IDL.Text], [Result], []),
  });
};
export const init = ({ IDL }) => { return []; };

import Cycles "mo:base/ExperimentalCycles";
import TrieMap "mo:base/TrieMap";
import Hash "mo:base/Hash";
import HashMap "mo:base/HashMap";
import Principal "mo:base/Principal";
import Result "mo:base/Result";
import Option "mo:base/Option";
import Time "mo:base/Time";
import Types "./types";
import Utils "./utils";
import Iter "mo:base/Iter";
import Buffer "mo:base/Buffer";
import Array "mo:base/Array";
import Text "mo:base/Text";
import Nat32 "mo:base/Nat32";
import Nat "mo:base/Nat";

actor Seachan {

    stable var visitCount : Nat32 = 0;
    public shared ({ caller }) func incrementVisitCount() {
        visitCount := visitCount + 1
    };
    public query ({ caller }) func getVisitCount() : async Nat32 {
        return visitCount
    };

    private var userStore : HashMap.HashMap<Principal, Types.User> = HashMap.HashMap<Principal, Types.User>(0, Principal.equal, Principal.hash);
    // private stable var userEntries : [(Principal, Types.User)] = [];

    public query ({ caller }) func getUsers() : async Result.Result<[Types.User], Types.Error> {
        assert Utils.isAdmin(caller);
        let entries : Iter.Iter<(Principal, Types.User)> = userStore.entries();
        let iter : Iter.Iter<Types.User> = Iter.map(entries, func((key : Principal, value : Types.User)) : Types.User { value });
        #ok(Iter.toArray(iter))
    };

    public query func getUserCount() : async Nat { userStore.size() };

    public shared ({ caller }) func getUser(userPrincipal : Principal) : async Result.Result<Types.User, Types.Error> {
        assert Utils.isAdmin(caller);
        assert not Principal.isAnonymous(userPrincipal);
        let user : ?Types.User = userStore.get(userPrincipal);
        let userExists = Option.isSome(?user);
        switch (user) {
            case (?userExists) {
                #ok(userExists)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func getOrCreateUser(principalProvider : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                #ok(exists)
            };
            case (null) {
                var badges : [Text] = [];
                if (Utils.isAdmin(caller)) { badges := ["Admin"] };
                var newUser = {
                    principal = caller;
                    usernames = [];
                    badges = badges;
                    timestamp = Time.now();
                    principalProvider = principalProvider;
                    navbarBoards = [];
                    preferences = {
                        showNsfw = false;
                        showFlagged = false;
                        boardView = "paged";
                        threadView = "paged";
                        directMessaging = false
                    };
                    savedPosts = {
                        watchedThreads = [];
                        minimizedPosts = [];
                        hiddenPosts = []
                    }
                } : Types.User;
                userStore.put(caller, newUser);
                #ok(newUser)
            }
        }
    };

    public shared ({ caller }) func claimUsername(usernameIn : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let users = userStore.vals();
        for (user in users) {
            for (userName in Iter.fromArray(user.usernames)) {
                if (userName == usernameIn) { return #err(#AlreadyExists) }
            }
        };

        // fetch and assign the username to the user if it is unique
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                let usernameBuffer : Buffer.Buffer<Text> = Buffer.fromArray(exists.usernames);
                usernameBuffer.add(usernameIn);
                var updatedUser = {
                    principal = exists.principal;
                    usernames = Buffer.toArray(usernameBuffer) : [Text];
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = exists.preferences;
                    savedPosts = exists.savedPosts
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func deleteUsername(usernameIn : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var updatedUsernames = Array.filter(exists.usernames, func(v : Text) : Bool { v != usernameIn });
                var updatedUser = {
                    principal = exists.principal;
                    usernames = updatedUsernames : [Text];
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = exists.preferences;
                    savedPosts = exists.savedPosts
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func addNavbarBoard(boardIdIn : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                let navbarBoardBuffer : Buffer.Buffer<Text> = Buffer.fromArray(exists.navbarBoards);
                navbarBoardBuffer.add(boardIdIn);
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = Buffer.toArray(navbarBoardBuffer);
                    preferences = exists.preferences;
                    savedPosts = exists.savedPosts
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func deleteNavbarBoard(boardIdIn : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var updatedNavBarBoards = Array.filter(exists.navbarBoards, func(v : Text) : Bool { v != boardIdIn });
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = updatedNavBarBoards;
                    preferences = exists.preferences;
                    savedPosts = exists.savedPosts
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func toggleWatchedThread(postKey : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        assert exists;
        switch (entry) {
            case (?exists) {
                var watchedThreads = [] : [Text];
                let watchedThreadMatchBuffer : Buffer.Buffer<Text> = Buffer.fromArray(Array.filter(exists.savedPosts.watchedThreads, func(v : Text) : Bool { v == postKey }));
                if (watchedThreadMatchBuffer.size() > 0) {
                    watchedThreads := Array.filter(exists.savedPosts.watchedThreads, func(v : Text) : Bool { v != postKey })
                } else {
                    let watchThreadBuffer : Buffer.Buffer<Text> = Buffer.fromArray(exists.savedPosts.watchedThreads);
                    watchThreadBuffer.add(postKey);
                    watchedThreads := Buffer.toArray(watchThreadBuffer) : [Text]
                };
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = exists.preferences;
                    savedPosts = {
                        watchedThreads = watchedThreads;
                        hiddenPosts = exists.savedPosts.hiddenPosts;
                        minimizedPosts = exists.savedPosts.minimizedPosts;

                    }
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func toggleMinimizedPost(postKey : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var minimizedPosts = [] : [Text];
                let minimizedPostMatchBuffer : Buffer.Buffer<Text> = Buffer.fromArray(Array.filter(exists.savedPosts.minimizedPosts, func(v : Text) : Bool { v == postKey }));
                if (minimizedPostMatchBuffer.size() > 0) {
                    minimizedPosts := Array.filter(exists.savedPosts.minimizedPosts, func(v : Text) : Bool { v != postKey })
                } else {
                    let minimizedPostBuffer : Buffer.Buffer<Text> = Buffer.fromArray(exists.savedPosts.minimizedPosts);
                    minimizedPostBuffer.add(postKey);
                    minimizedPosts := Buffer.toArray(minimizedPostBuffer) : [Text]
                };
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = exists.preferences;
                    savedPosts = {
                        watchedThreads = exists.savedPosts.watchedThreads;
                        hiddenPosts = exists.savedPosts.hiddenPosts;
                        minimizedPosts = minimizedPosts
                    }
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func toggleHiddenPost(postKey : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var hiddenPosts = [] : [Text];
                let hiddenPostMatchBuffer : Buffer.Buffer<Text> = Buffer.fromArray(Array.filter(exists.savedPosts.hiddenPosts, func(v : Text) : Bool { v == postKey }));
                if (hiddenPostMatchBuffer.size() > 0) {
                    hiddenPosts := Array.filter(exists.savedPosts.hiddenPosts, func(v : Text) : Bool { v != postKey })
                } else {
                    let hiddenPostBuffer : Buffer.Buffer<Text> = Buffer.fromArray(exists.savedPosts.hiddenPosts);
                    hiddenPostBuffer.add(postKey);
                    hiddenPosts := Buffer.toArray(hiddenPostBuffer) : [Text]
                };
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = exists.preferences;
                    savedPosts = {
                        watchedThreads = exists.savedPosts.watchedThreads;
                        hiddenPosts = hiddenPosts;
                        minimizedPosts = exists.savedPosts.minimizedPosts
                    }
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func clearWatchedThreads() : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = exists.preferences;
                    savedPosts = {
                        watchedThreads = [];
                        hiddenPosts = exists.savedPosts.hiddenPosts;
                        minimizedPosts = exists.savedPosts.minimizedPosts
                    }
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func toggleShowNsfw() : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = {
                        showNsfw = not exists.preferences.showNsfw;
                        showFlagged = exists.preferences.showFlagged;
                        boardView = exists.preferences.boardView;
                        threadView = exists.preferences.threadView;
                        directMessaging = exists.preferences.directMessaging
                    };
                    savedPosts = exists.savedPosts
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func toggleShowFlagged() : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = {
                        showNsfw = exists.preferences.showNsfw;
                        showFlagged = not exists.preferences.showFlagged;
                        boardView = exists.preferences.boardView;
                        threadView = exists.preferences.threadView;
                        directMessaging = exists.preferences.directMessaging
                    };
                    savedPosts = exists.savedPosts

                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func setBoardViewPreference(boardView : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = {
                        showNsfw = exists.preferences.showNsfw;
                        showFlagged = exists.preferences.showFlagged;
                        boardView = boardView;
                        threadView = exists.preferences.threadView;
                        directMessaging = exists.preferences.directMessaging
                    };
                    savedPosts = exists.savedPosts

                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func setThreadViewPreference(threadView : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = exists.badges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = {
                        showNsfw = exists.preferences.showNsfw;
                        showFlagged = exists.preferences.showFlagged;
                        boardView = exists.preferences.boardView;
                        threadView = threadView;
                        directMessaging = exists.preferences.directMessaging
                    };
                    savedPosts = exists.savedPosts
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    // public shared ({ caller }) func toggleEnableDirectMessaging() : async Result.Result<Types.User, Types.Error> {
    //     assert not Principal.isAnonymous(caller);
    //     let entry : ?Types.User = userStore.get(caller);
    //     let exists = Option.isSome(?entry);
    //     switch (entry) {
    //         case (?exists) {
    //             var updatedUser = {
    //                 principal = exists.principal;
    //                 usernames = exists.usernames;
    //                 badges = exists.badges;
    //                 timestamp = exists.timestamp;
    //                 principalProvider = exists.principalProvider;
    //                 navbarBoards = exists.navbarBoards;
    //                 preferences = {
    //                     showNsfw = exists.preferences.showNsfw;
    //                     showFlagged = exists.preferences.showFlagged;
    //                     boardView = exists.preferences.boardView;
    //                     threadView = exists.preferences.threadView;
    //                     directMessaging = not exists.preferences.directMessaging
    //                 };
    //                 savedPosts = exists.savedPosts
    //             } : Types.User;
    //             userStore.put(updatedUser.principal, updatedUser);
    //             #ok(updatedUser)
    //         };
    //         case (null) { #err(#NotFound) }
    //     }
    // };

    public shared ({ caller }) func addBadge(badgeName : Text) : async Result.Result<Types.User, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let entry : ?Types.User = userStore.get(caller);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var userBadges = [] : [Text];
                let userBadgesBuffer : Buffer.Buffer<Text> = Buffer.fromArray(Array.filter(exists.badges, func(v : Text) : Bool { v == badgeName }));
                if (userBadgesBuffer.size() == 0) {
                    let newUserBadgeBuffer : Buffer.Buffer<Text> = Buffer.fromArray(exists.badges);
                    newUserBadgeBuffer.add(badgeName);
                    userBadges := Buffer.toArray(newUserBadgeBuffer) : [Text]
                };
                var updatedUser = {
                    principal = exists.principal;
                    usernames = exists.usernames;
                    badges = userBadges;
                    timestamp = exists.timestamp;
                    principalProvider = exists.principalProvider;
                    navbarBoards = exists.navbarBoards;
                    preferences = exists.preferences;
                    savedPosts = exists.savedPosts
                } : Types.User;
                userStore.put(updatedUser.principal, updatedUser);
                #ok(updatedUser)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public query ({ caller }) func getPrincipal() : async Text {
        Principal.toText(caller)
    };

    public shared ({ caller }) func deleteAllUsers() : async Result.Result<Text, Types.Error> {
        assert Utils.isAdmin(caller);
        let keys = userStore.keys();
        for (key in keys) { userStore.delete(key) };
        #ok("Deleted all users")
    };

    public shared ({ caller }) func importUsers(inUsers : [Types.User]) : async Result.Result<[Types.User], Types.Error> {
        assert Utils.isAdmin(caller);
        for (user in Iter.fromArray(inUsers)) {
            userStore.put(user.principal, user)
        };
        let entries : Iter.Iter<(Principal, Types.User)> = userStore.entries();
        let iter : Iter.Iter<Types.User> = Iter.map(entries, func((key : Principal, value : Types.User)) : Types.User { value });
        #ok(Iter.toArray(iter))
    };

    public shared ({ caller }) func importIPs(inIPs : [Types.IPAddress]) : async Result.Result<[Types.IPAddress], Types.Error> {
        assert Utils.isAdmin(caller);
        for (ip in Iter.fromArray(inIPs)) {
            ipStore.put(ip.postId, ip)
        };
        let entries : Iter.Iter<(Text, Types.IPAddress)> = ipStore.entries();
        let iter : Iter.Iter<Types.IPAddress> = Iter.map(entries, func((key : Text, value : Types.IPAddress)) : Types.IPAddress { value });
        #ok(Iter.toArray(iter))
    };

    // boards
    private var boardStore : TrieMap.TrieMap<Text, Types.Board> = TrieMap.TrieMap<Text, Types.Board>(Text.equal, Text.hash);

    public query func boardExists(abbreviation : Text) : async Bool {
        let entry : ?Types.Board = boardStore.get(abbreviation);
        Option.isSome(entry)
    };

    public query ({ caller }) func getListedBoards() : async Result.Result<[Types.Board], Types.Error> {
        let entries : Iter.Iter<(Text, Types.Board)> = boardStore.entries();
        let iter : Iter.Iter<Types.Board> = Iter.map(entries, func((key : Text, value : Types.Board)) : Types.Board { value });
        let arrayFiltered = Array.filter(Iter.toArray(iter), func(v : Types.Board) : Bool { v.properties.unListed == false });
        #ok(arrayFiltered)
    };

    public query func getBoard(id : Text) : async Result.Result<Types.Board, Types.Error> {
        let entry : ?Types.Board = boardStore.get(id);
        let exists = Option.isSome(entry);
        switch (entry) {
            case (?exists) { #ok(exists) };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func deleteBoard(id : Text) : async Result.Result<Text, Types.Error> {
        assert Utils.isAdmin(caller);
        let boardEntry : ?Types.Board = boardStore.get(id);
        let boardExists = Option.isSome(boardEntry);
        switch (boardEntry) {
            case (?boardExists) {
                boardStore.delete(id);
                let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
                let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
                let boardPosts = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.id == id });
                for (post in Iter.fromArray(boardPosts)) {
                    postStore.delete(post.id)
                };
                return #ok("Board " # id # " deleted")
            };
            case (null) {
                #err(#NotFound)
            }
        }
    };

    public query func getBoardCount() : async Nat { boardStore.size() };

    public shared query func getNextPostId(boardId : Text) : async Result.Result<Nat32, Types.Error> {
        let entry : ?Types.Board = boardStore.get(boardId);
        let exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                #ok(exists.postCount : Nat32)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func createBoard(inBoard : Types.BoardCreate) : async Result.Result<Types.Board, Types.CreateError> {
        // assert not Principal.isAnonymous(caller);
        assert Utils.isAdmin(caller);
        if (await boardExists(inBoard.id)) {
            return #err(#AlreadyExists)
        };
        var newBoard = {
            id = inBoard.id;
            name = inBoard.name;
            properties = inBoard.properties;
            gate = inBoard.gate;
            ownerPrincipal = Principal.toText(caller);
            timestamp = Time.now();
            bumpTimestamp = Time.now();
            postCount = 0;
            threadCount = 0;
            fileCount = 0;
            fileSize = 0;
            views = 0
        } : Types.Board;
        boardStore.put(inBoard.id, newBoard);
        #ok(newBoard)
    };

    public query ({ caller }) func getBoards() : async Result.Result<[Types.Board], Types.Error> {
        assert Utils.isAdmin(caller);
        let entries : Iter.Iter<(Text, Types.Board)> = boardStore.entries();
        let iter : Iter.Iter<Types.Board> = Iter.map(entries, func((key : Text, value : Types.Board)) : Types.Board { value });
        #ok(Iter.toArray(iter))
    };

    public shared ({ caller }) func deleteAllBoards() : async Result.Result<Text, Types.Error> {
        assert Utils.isAdmin(caller);
        let keys = boardStore.keys();
        for (key in keys) { boardStore.delete(key) };
        let postKeys = postStore.keys();
        for (postKey in postKeys) {
            postStore.delete(postKey)
        };
        #ok("Deleted all boards and posts")
    };

    public shared ({ caller }) func importBoards(inBoards : [Types.Board]) : async Result.Result<[Types.Board], Types.Error> {
        assert Utils.isAdmin(caller);
        for (board in Iter.fromArray(inBoards)) {
            boardStore.put(board.id, board)
        };
        let entries : Iter.Iter<(Text, Types.Board)> = boardStore.entries();
        let iter : Iter.Iter<Types.Board> = Iter.map(entries, func((key : Text, value : Types.Board)) : Types.Board { value });
        #ok(Iter.toArray(iter))
    };

    public shared ({ caller }) func importNewBoards(inBoards : [Types.Board]) : async Result.Result<[Types.Board], Types.Error> {
        assert Utils.isAdmin(caller);
        let newBoardBuffer : Buffer.Buffer<Types.Board> = Buffer.fromArray([]);
        for (board in Iter.fromArray(inBoards)) {
            switch (await createBoard(board)) {
                case (#err(#AlreadyExists)) {};
                case (#ok(board)) {
                    newBoardBuffer.add(board)
                }
            }
        };
        #ok(Buffer.toArray(newBoardBuffer))
    };

    // posts
    private var postStore : TrieMap.TrieMap<Text, Types.Post> = TrieMap.TrieMap<Text, Types.Post>(Text.equal, Text.hash);

    public query func getPostCount() : async Nat { postStore.size() };

    // public shared query ({ caller }) func getPublicPost(postKey : Text) : async Result.Result<Types.Post, Types.Error> {
    //     let post : ?Types.Post = postStore.get(postKey);
    //     let postExists = Option.isSome(?post);
    //     switch (post) {
    //         case (?postExists) {
    //             let updatedPost = {
    //                 id = postExists.id;
    //                 threadId = postExists.threadId;
    //                 boardId = postExists.boardId;
    //                 subject = postExists.subject;
    //                 body = postExists.body;
    //                 timestamp = postExists.timestamp;
    //                 bumpTimestamp = postExists.bumpTimestamp;
    //                 web3 = postExists.web3;
    //                 poster = {
    //                     posterId = postExists.poster.posterId;
    //                     ownerPrincipal = Principal.fromText("2vxsx-fae");
    //                     badges = postExists.poster.badges;
    //                     directMessage = postExists.poster.directMessage;
    //                     ipAddress = postExists.poster.ipAddress;
    //                     flag = postExists.poster.flag;
    //                     country = postExists.poster.country
    //                 };
    //                 threadDetails = postExists.threadDetails;
    //                 reports = postExists.reports;
    //                 properties = postExists.properties;
    //                 file = postExists.file;
    //                 gate = postExists.gate;
    //                 poll = postExists.poll
    //             };
    //             #ok(updatedPost)
    //         };
    //         case (null) { #err(#NotFound) }
    //     }
    // };

    public shared query ({ caller }) func getPost(postKey : Text) : async Result.Result<Types.Post, Types.Error> {
        let post : ?Types.Post = postStore.get(postKey);
        let postExists = Option.isSome(?post);
        switch (post) {
            case (?postExists) {
                #ok(postExists)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public query ({ caller }) func getPostsFromIds(postKeyArray : [Text]) : async Result.Result<[Types.Post], Types.Error> {
        let newPostBuffer : Buffer.Buffer<Types.Post> = Buffer.fromArray([]);
        for (postKey in Iter.fromArray(postKeyArray)) {
            let post : ?Types.Post = postStore.get(postKey);
            let postExists = Option.isSome(?post);
            switch (post) {
                case (?postExists) {
                    newPostBuffer.add(postExists)
                };
                case (null) { return #err(#NotFound) }
            }
        };
        return #ok(Buffer.toArray(newPostBuffer) : [Types.Post])
    };

    public query func getThreadCount() : async Nat {
        let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
        let threads = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.properties.op == true });
        threads.size()
    };

    public query func getFileCount() : async Nat {
        let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
        let threads = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.file.path != "" });
        threads.size()
    };

    public query ({ caller }) func getFileUploadSize() : async Nat32 {
        let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
        var fileUploadSize = 0 : Nat32;
        for (post in allPostsIter) {
            fileUploadSize := fileUploadSize + post.file.size
        };
        return fileUploadSize
    };

    public query ({ caller }) func getPosts() : async Result.Result<[Types.Post], Types.Error> {
        #ok(Iter.toArray(postStore.vals()))
    };

    public query ({ caller }) func getBoardThreadCount(key : Text) : async Nat {
        let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
        let threads = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.boardId == key and v.properties.op == true });
        threads.size()
    };

    public query ({ caller }) func getBoardPostCount(key : Text) : async Nat {
        let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
        let posts = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.boardId == key });
        posts.size()
    };

    public query ({ caller }) func getBoardFileUploadSize(key : Text) : async Nat32 {
        let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
        let posts = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.boardId == key });
        var fileUploadSize = 0 : Nat32;
        for (post in Iter.fromArray(posts)) {
            fileUploadSize := fileUploadSize + post.file.size
        };
        return fileUploadSize
    };

    public query ({ caller }) func getLatestThreads(key : Text) : async Nat32 {
        let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
        let posts = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.boardId == key });
        var fileUploadSize = 0 : Nat32;
        for (post in Iter.fromArray(posts)) {
            fileUploadSize := fileUploadSize + post.file.size
        };
        return fileUploadSize
    };

    public shared ({ caller }) func deleteAllPosts() : async Result.Result<Text, Types.Error> {
        assert Utils.isAdmin(caller);
        let postKeys = postStore.keys();
        for (postKey in postKeys) {
            postStore.delete(postKey)
        };
        let boardKeys = boardStore.keys();
        #ok("Deleted all posts")
    };

    public query func getThreads() : async Result.Result<[Types.Post], Types.Error> {
        let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
        let threads = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.properties.op == true });
        #ok(threads)
    };

    // add gate checks
    public query func getBoardThreads(key : Text) : async Result.Result<[Types.Post], Types.Error> {
        let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let allPostsIter : Iter.Iter<Types.Post> = Iter.map(
            allPosts,
            func((key : Text, value : Types.Post)) : Types.Post { value },
        );
        let threads = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.boardId == key and v.properties.op == true });
        #ok(threads)
    };

    // public query func getBoardPosts(key : Text) : async Result.Result<[Types.Post], Types.Error> {
    //     let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
    //     let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
    //     let threads = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.boardId == key });
    //     #ok(threads)
    // };

    public shared ({ caller }) func incrementBoardViewCount(boardId : Text) {
        let board : ?Types.Board = boardStore.get(boardId);
        switch (board) {
            case (?exists) {
                var updatedBoard = {
                    id = exists.id;
                    name = exists.name;
                    properties = exists.properties;
                    gate = exists.gate;
                    ownerPrincipal = exists.ownerPrincipal;
                    timestamp = exists.timestamp;
                    bumpTimestamp = exists.bumpTimestamp;
                    postCount = exists.postCount;
                    threadCount = exists.threadCount;
                    fileCount = exists.fileCount;
                    fileSize = exists.fileSize;
                    views = exists.views + 1
                } : Types.Board;
                boardStore.put(exists.id, updatedBoard)
            };
            case (null) {}
        }
    };

    public shared ({ caller }) func incrementThreadViewCount(threadId : Text) {
        let thread : ?Types.Post = postStore.get(threadId);
        switch (thread) {
            case (?exists) {
                var updatedThread = {
                    id = exists.id;
                    threadId = exists.threadId;
                    boardId = exists.boardId;
                    subject = exists.subject;
                    body = exists.body;
                    timestamp = exists.timestamp;
                    bumpTimestamp = exists.bumpTimestamp;
                    web3 = exists.web3;
                    poster = exists.poster;
                    reports = exists.reports;
                    properties = exists.properties;
                    threadDetails = {
                        postIds = exists.threadDetails.postIds;
                        fileCount = exists.threadDetails.fileCount;
                        fileSize = exists.threadDetails.fileSize;
                        uniquePosters = exists.threadDetails.uniquePosters;
                        views = exists.threadDetails.views + 1
                    };
                    file = exists.file;
                    gate = exists.gate;
                    poll = exists.poll

                } : Types.Post;
                postStore.put(updatedThread.id, updatedThread)
            };
            case (null) {}
        }
    };

    public query func getThread(boardId : Text, threadId : Text) : async Result.Result<[Types.Post], Types.Error> {
        let allPosts : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let allPostsIter : Iter.Iter<Types.Post> = Iter.map(allPosts, func((key : Text, value : Types.Post)) : Types.Post { value });
        let thread = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.boardId == boardId and v.threadId == threadId });
        let op = Array.filter(thread, func(v : Types.Post) : Bool { v.properties.op == true });
        #ok(thread)
    };

    // TODO add backend gate checks
    public shared ({ caller }) func addPostToBoard(boardId : Text, newPost : Types.Post) {
        let boardEntry : ?Types.Board = boardStore.get(newPost.boardId);
        let boardExists = Option.isSome(?boardEntry);
        switch (boardEntry) {
            case (?boardExists) {
                let newThreadCount = if (newPost.properties.op) {
                    boardExists.threadCount + 1
                } else { boardExists.threadCount };
                let newFileCount = if (newPost.file.path != "") {
                    boardExists.fileCount + 1
                } else { boardExists.fileCount };
                let newFileSize = if (newPost.file.size != 0) {
                    boardExists.fileSize + newPost.file.size
                } else { boardExists.fileSize };
                var updatedBoard = {
                    id = boardExists.id;
                    name = boardExists.name;
                    properties = boardExists.properties;
                    gate = boardExists.gate;
                    ownerPrincipal = boardExists.ownerPrincipal;
                    timestamp = boardExists.timestamp;
                    bumpTimestamp = newPost.timestamp;
                    postCount = boardExists.postCount + 1;
                    threadCount = newThreadCount;
                    fileCount = newFileCount;
                    fileSize = newFileSize;
                    views = boardExists.views
                } : Types.Board;
                boardStore.put(boardExists.id, updatedBoard)
            };
            case (null) {}
        }
    };

    // TODO add backend gate checks
    public shared ({ caller }) func addPostToThread(threadId : Text, newPost : Types.Post) {
        let thread : ?Types.Post = postStore.get(threadId);
        switch (thread) {
            case (?exists) {
                // replies
                let replyBuffer : Buffer.Buffer<Text> = Buffer.fromArray<Text>(exists.threadDetails.postIds);
                replyBuffer.add(newPost.id);
                let postArray = Buffer.toArray(replyBuffer) : [Text];
                // file count
                let newFileCount = if (newPost.file.path != "") {
                    exists.threadDetails.fileCount + 1
                } else { exists.threadDetails.fileCount };
                // file size
                let newFileSize = if (newPost.file.size != 0) {
                    exists.threadDetails.fileSize + newPost.file.size
                } else { exists.threadDetails.fileSize };
                // posters
                let posterBuffer : Buffer.Buffer<Types.PosterId> = Buffer.fromArray(exists.threadDetails.uniquePosters);
                let posterMatchBuffer : Buffer.Buffer<Types.PosterId> = Buffer.fromArray(Array.filter(exists.threadDetails.uniquePosters, func(v : Types.PosterId) : Bool { v == newPost.poster.posterId }));
                if (posterMatchBuffer.size() == 0) {
                    posterBuffer.add(newPost.poster.posterId)
                };
                // sage
                let newBumpTimestamp = if (newPost.properties.saged) {
                    exists.bumpTimestamp
                } else { newPost.timestamp };
                var updatedThread = {
                    id = exists.id;
                    threadId = exists.threadId;
                    boardId = exists.boardId;
                    subject = exists.subject;
                    body = exists.body;
                    timestamp = exists.timestamp;
                    bumpTimestamp = newBumpTimestamp;
                    web3 = exists.web3;
                    poster = exists.poster;
                    reports = exists.reports;
                    properties = exists.properties;
                    threadDetails = {
                        postIds = postArray;
                        fileCount = newFileCount;
                        fileSize = newFileSize;
                        uniquePosters = Buffer.toArray(posterBuffer);
                        views = exists.threadDetails.views
                    };
                    file = exists.file;
                    gate = exists.gate;
                    poll = exists.poll
                } : Types.Post;
                postStore.put(exists.id, updatedThread)
            };
            case (null) {}
        }
    };

    public shared ({ caller }) func createPost(inPost : Types.PostCreate) : async Result.Result<Types.Post, Types.CreatePostError> {
        assert Array.filter(bannedIPs, func(v : Text) : Bool { v == inPost.poster.ipAddress }).size() == 0;
        let threadId = inPost.threadId;
        let timestamp = Time.now();
        var newPost = {
            id = inPost.id;
            threadId = inPost.threadId;
            boardId = inPost.boardId;
            subject = inPost.subject;
            body = inPost.body;
            timestamp = timestamp;
            bumpTimestamp = timestamp;
            web3 = {
                icpBalance = inPost.web3.icpBalance;
                icpReceived = 0.00;
                acceptIcp = inPost.web3.acceptIcp
            };
            poster = {
                posterId = inPost.poster.posterId;
                // ownerPrincipal = Principal.toText(caller);
                badges = inPost.poster.badges;
                directMessage = inPost.poster.directMessage;
                // ipAddress = inPost.poster.ipAddress;
                flag = inPost.poster.flag;
                country = inPost.poster.country
            };
            reports = {
                seaChadPrincipals = [] : [Principal];
                count = 0 : Nat32
            };
            properties = {
                op = inPost.properties.op;
                stickied = inPost.properties.stickied;
                locked = inPost.properties.locked;
                flagged = false;
                saged = inPost.properties.saged
            };
            threadDetails = {
                postIds = [inPost.id];
                fileCount = 1;
                fileSize = inPost.file.size;
                uniquePosters = [inPost.poster.posterId];
                views = 0
            };
            file = inPost.file;
            gate = inPost.gate;
            poll = inPost.poll
        } : Types.Post;
        // add new post to store
        postStore.put(inPost.id, newPost);
        // update board
        addPostToBoard(inPost.boardId, newPost);
        //update thread
        if (inPost.properties.op == false) {
            addPostToThread(inPost.threadId, newPost)
        };
        // add ip to store
        let ipStoreEntry = {
            postId = inPost.id;
            ipAddress = inPost.poster.ipAddress
        };
        ipStore.put(inPost.id, ipStoreEntry);
        // return
        #ok(newPost)
    };

    public shared ({ caller }) func reportPost(postKey : Text, reportLimit : Nat) : async Result.Result<Types.Post, Types.Error> {
        let post : ?Types.Post = postStore.get(postKey);
        let exists = Option.isSome(?post);
        switch (post) {
            case (?exists) {
                var seaChadPrincipals = exists.reports.seaChadPrincipals;
                var flagged = exists.properties.flagged;
                if (not Principal.isAnonymous(caller)) {
                    let userEntry : ?Types.User = userStore.get(caller);
                    let userExists = Option.isSome(?userEntry);
                    switch (userEntry) {
                        case (?userExists) {
                            let userBadgeBuffer : Buffer.Buffer<Text> = Buffer.fromArray(userExists.badges);
                            var isSeaChad = false;
                            for (badge in Iter.fromArray(userExists.badges)) {
                                if (badge == "SeaChad") { isSeaChad := true }
                            };
                            if (isSeaChad) {
                                let seaChadPrincipalsBuffer : Buffer.Buffer<Principal> = Buffer.fromArray(seaChadPrincipals);
                                let seaChadPrincipalsMatchBuffer : Buffer.Buffer<Principal> = Buffer.fromArray(Array.filter(seaChadPrincipals, func(v : Principal) : Bool { v == caller }));
                                if (seaChadPrincipalsMatchBuffer.size() == 0) {
                                    seaChadPrincipalsBuffer.add(caller);
                                    seaChadPrincipals := Buffer.toArray(seaChadPrincipalsBuffer);
                                    if (seaChadPrincipalsBuffer.size() > reportLimit) {
                                        flagged := true
                                    }
                                } else {
                                    return #err(#AlreadyReported)
                                }
                            }
                        };
                        case (null) {}
                    }
                };

                var updatedPost = {
                    id = exists.id;
                    threadId = exists.threadId;
                    boardId = exists.boardId;
                    subject = exists.subject;
                    body = exists.body;
                    timestamp = exists.timestamp;
                    bumpTimestamp = exists.bumpTimestamp;
                    web3 = exists.web3;
                    poster = exists.poster;
                    reports = {
                        seaChadPrincipals = seaChadPrincipals;
                        count = exists.reports.count + 1 : Nat32
                    };
                    properties = {
                        op = exists.properties.op;
                        stickied = exists.properties.stickied;
                        locked = exists.properties.locked;
                        flagged = flagged;
                        saged = exists.properties.saged
                    };
                    threadDetails = exists.threadDetails;
                    file = exists.file;
                    gate = exists.gate;
                    poll = exists.poll
                } : Types.Post;

                postStore.put(postKey, updatedPost);
                #ok(updatedPost)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func removePost(postKey : Text) : async Result.Result<Types.Post, Types.Error> {
        assert Utils.isAdmin(caller);
        let entry : ?Types.Post = postStore.get(postKey);
        var exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var isFileNfw = not exists.file.nsfw;
                var updatedPost = {
                    id = exists.id;
                    threadId = exists.threadId;
                    boardId = exists.boardId;
                    subject = exists.subject;
                    body = exists.body;
                    timestamp = exists.timestamp;
                    bumpTimestamp = exists.bumpTimestamp;
                    web3 = exists.web3;
                    poster = exists.poster;
                    reports = exists.reports;
                    properties = {
                        op = exists.properties.op;
                        stickied = exists.properties.stickied;
                        locked = exists.properties.locked;
                        flagged = true;
                        saged = exists.properties.saged
                    };
                    threadDetails = exists.threadDetails;
                    file = exists.file;
                    gate = exists.gate;
                    poll = exists.poll
                } : Types.Post;
                postStore.put(postKey, updatedPost);
                #ok(updatedPost)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func toggleNSFW(postKey : Text) : async Result.Result<Types.Post, Types.Error> {
        assert Utils.isAdmin(caller);
        let entry : ?Types.Post = postStore.get(postKey);
        var exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                var isFileNsfw = not exists.file.nsfw;
                var updatedPost = {
                    id = exists.id;
                    threadId = exists.threadId;
                    boardId = exists.boardId;
                    subject = exists.subject;
                    body = exists.body;
                    timestamp = exists.timestamp;
                    bumpTimestamp = exists.bumpTimestamp;
                    web3 = exists.web3;
                    poster = exists.poster;
                    reports = exists.reports;
                    properties = exists.properties;
                    threadDetails = exists.threadDetails;
                    file = {
                        path = exists.file.path;
                        nsfw = not exists.file.nsfw;
                        format = exists.file.format;
                        name = exists.file.name;
                        size = exists.file.size
                    } : Types.PostFile;
                    gate = exists.gate;
                    poll = exists.poll
                } : Types.Post;
                postStore.put(postKey, updatedPost);
                #ok(updatedPost)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func importPosts(inPosts : [Types.Post]) : async Result.Result<[Types.Post], Types.Error> {
        assert Utils.isAdmin(caller);
        for (post in Iter.fromArray(inPosts)) {
            var newPost = {
                id = post.id;
                boardId = post.boardId;
                threadId = post.threadId;
                subject = post.subject;
                body = post.body;
                timestamp = post.timestamp;
                bumpTimestamp = post.bumpTimestamp;
                web3 = post.web3;
                poster = post.poster;
                reports = post.reports;
                properties = post.properties;
                file = post.file;
                gate = post.gate;
                threadDetails = post.threadDetails;
                poll = post.poll
            } : Types.Post;
            postStore.put(post.id, newPost)
        };
        let entries : Iter.Iter<(Text, Types.Post)> = postStore.entries();
        let iter : Iter.Iter<Types.Post> = Iter.map(entries, func((key : Text, value : Types.Post)) : Types.Post { value });
        #ok(Iter.toArray(iter))
    };

    public shared ({ caller }) func importPost(inPost : Types.Post) : async Result.Result<Types.Post, Types.Error> {
        assert Utils.isAdmin(caller);
        postStore.put(inPost.id, inPost);
        #ok(inPost)
    };

    public shared ({ caller }) func importNewPosts(inPosts : [Types.PostCreate]) : async Result.Result<[Types.Post], Types.Error> {
        assert Utils.isAdmin(caller);
        let newPostBuffer : Buffer.Buffer<Types.Post> = Buffer.fromArray([]);
        for (post in Iter.fromArray(inPosts)) {
            switch (await createPost(post)) {
                case (#err(#BoardNotFound)) {};
                case (#ok(post)) {
                    newPostBuffer.add(post)
                }
            }
        };
        #ok(Buffer.toArray(newPostBuffer))
    };

    // messages
    private var conversationStore : HashMap.HashMap<Principal, HashMap.HashMap<Principal, [Types.Message]>> = HashMap.HashMap<Principal, HashMap.HashMap<Principal, [Types.Message]>>(0, Principal.equal, Principal.hash);

    public shared ({ caller }) func createMessage(inMessage : Types.MessageCreate) : async Result.Result<Types.Message, Types.CreateMessageError> {
        assert not Principal.isAnonymous(caller);
        // get existing messages
        let userEntry : ?Types.User = userStore.get(caller);
        var userExists = Option.isSome(?userEntry);
        switch (userEntry) {
            case (?userExists) {
                let newMessage = {
                    subject = inMessage.subject;
                    body = inMessage.body;
                    timestamp = Time.now() : Int
                };
                let conversationsEntry = conversationStore.get(caller);
                var conversationsExists = Option.isSome(?conversationsEntry);
                switch (conversationsEntry) {
                    case (?conversationsExists) {
                        let toMessageEntries : ?[Types.Message] = conversationsExists.get(inMessage.toPrincipal);
                        let toMessagesExist = Option.isSome(?toMessageEntries);
                        switch (toMessageEntries) {
                            case (?toMessagesExist) {
                                var updatedMessageBuffer : Buffer.Buffer<Types.Message> = Buffer.fromArray(toMessagesExist);
                                updatedMessageBuffer.add(newMessage);
                                conversationsExists.put(inMessage.toPrincipal, Buffer.toArray(updatedMessageBuffer));
                                conversationStore.put(caller, conversationsExists)
                            };
                            case (null) {
                                conversationsExists.put(inMessage.toPrincipal, [newMessage]);
                                conversationStore.put(caller, conversationsExists)
                            }
                        };
                        #ok(newMessage)
                    };
                    case (null) {
                        let newConversation = HashMap.HashMap<Principal, [Types.Message]>(1, Principal.equal, Principal.hash);
                        newConversation.put(inMessage.toPrincipal, [newMessage]);
                        conversationStore.put(caller, newConversation);
                        #ok(newMessage)
                    }
                }
            };
            case (null) { #err(#UserNotFound) }
        }
    };

    // #err(#UserNotFound) // let conversationsEntry : ?[Types.Conversation] = conversationStore.get(caller);
    // var conversationsExist = Option.isSome(?conversationsEntry);

    // let newMessage = {
    //     subject = inMessage.subject;
    //     body = inMessage.body;
    //     timestamp = Time.now() : Int
    // };

    // switch (conversationsEntry) {
    //     case (?conversationsExist) {
    //         // iterate over conversations
    //         let updatedConversationsBuffer : Buffer.Buffer<Types.Conversation> = Buffer.fromArray([]);
    //         for (conversation in Iter.fromArray(conversationsExist)) {
    //             // add message to exiting conversation
    //             if (conversation.toPrincipal == inMessage.toPrincipal) {
    //                 var updatedMessageBuffer : Buffer.Buffer<Types.Message> = Buffer.fromArray(conversation.messages);
    //                 updatedMessageBuffer.add(newMessage);
    //                 let updatedConversation = {
    //                     fromPrincipal = caller;
    //                     fromPosterId = inMessage.fromPosterId;
    //                     toPrincipal = inMessage.toPrincipal;
    //                     toPosterId = inMessage.toPosterId;
    //                     postId = inMessage.postId;
    //                     messages = Buffer.toArray(updatedMessageBuffer)
    //                 };
    //                 updatedConversationsBuffer.add(updatedConversation)
    //             }

    //             else {
    //                 updatedConversationsBuffer.add(conversation)
    //             }
    //         };
    //         conversationStore.put(caller, Buffer.toArray(updatedConversationsBuffer) : [Types.Conversation]);
    //         return #ok(Buffer.toArray(updatedConversation))
    //     };
    //     case (null) {
    //         let newConversations = {
    //             fromPrincipal = caller;
    //             fromPosterId = inMessage.fromPosterId;
    //             toPrincipal = inMessage.toPrincipal;
    //             toPosterId = inMessage.toPosterId;
    //             postId = inMessage.postId;
    //             messages = [newMessage]
    //         };
    //         conversationStore.put(caller, [newConversations] : [Types.Conversation]);
    //         return #ok("")
    //     };

    // }

    // let messageEntry : ?[Types.Message] = messageStore.get(caller);
    // var messageExists = Option.isSome(?messageEntry);

    // switch (messageEntry) {
    //     case (?messageExists) {
    //         let existingMessageBuffer : Buffer.Buffer<Types.Message> = Buffer.fromArray(messageExists);
    //         existingMessageBuffer.add(newMessage);
    //         messageStore.put(caller, Buffer.toArray(existingMessageBuffer) : [Types.Message])
    //     };
    //     case (null) {
    //         messageStore.put(caller, [newMessage])
    //     }
    // };
    // #ok(newMessage)

    // public shared query ({ caller }) func getConversations() : async Result.Result<[Principal], Types.Error> {
    //     // asset isUser()
    //     let userEntry : ?Types.User = userStore.get(caller);
    //     var userExists = Option.isSome(?userEntry);
    //     switch (userEntry) {
    //         case (?userExists) {
    //             let conversationEntry : ?HashMap.HashMap<Principal, [Types.Message]> = conversationStore.get(caller);
    //             var conversationExists = Option.isSome(?conversationEntry);
    //             switch (conversationEntry) {
    //                 case (?conversationExists) {
    //                     let allConversations : Iter.Iter<(Principal)> = conversationExists.keys();
    //                     let principals = Iter.toArray(allConversations);
    //                     #ok(principals)
    //                 };
    //                 case (null) { #ok([]) }
    //             }
    //         };
    //         case (null) { #err(#NotFound) }
    //     };

    // };

    // public shared query ({ caller }) func getConversation(toPrincipal : Principal) : async Result.Result<[Types.Message], Types.Error> {
    //     assert not Principal.isAnonymous(caller);
    //     let userEntry : ?Types.User = userStore.get(caller);
    //     var userExists = Option.isSome(?userEntry);
    //     switch (userEntry) {
    //         case (?userExists) {
    //             let conversationEntry : ?HashMap.HashMap<Principal, [Types.Message]> = conversationStore.get(caller);
    //             var conversationExists = Option.isSome(?conversationEntry);
    //             switch (conversationEntry) {
    //                 case (?conversationExists) {
    //                     let messagesEntry : ?[Types.Message] = conversationExists.get(toPrincipal);
    //                     let messagesExist = Option.isSome(?messagesEntry);
    //                     switch (messagesEntry) {
    //                         case (?messagesExist) {
    //                             #ok(messagesExist)
    //                         };
    //                         case (null) { #err(#NotFound) }
    //                     }
    //                 };
    //                 case (null) { #err(#NotFound) }
    //             }
    //         };
    //         case (null) { #err(#NotFound) }
    //     }
    // };

    // let allConversationsIter : Iter.Iter<Principal> = Iter.map(allConversations, func((key : Principal)) : [Principal] { key });
    // let threads = Array.filter(Iter.toArray(allPostsIter), func(v : Types.Post) : Bool { v.boardId == key });
    // #ok(threads)

    // let conversationBuffer : Buffer.Buffer<Principal> = Buffer.fromArray([]);

    // var existsTest : HashMap.HashMap<Principal, [Types.Message]> = exists;
    // var existsVals : Iter.Iter<(Principal, [Types.Message])> = existsTest.entries();

    // for (choiceOption in Iter.fromArray(exists.poll.choices)) {

    // var existsArray : [Types.Message] = Iter.toArray(existsVals);
    // let entries : Iter.Iter<(Principal, Types.User)> = userStore.entries();
    // #ok("")

    public shared ({ caller }) func clearMessages() {
        assert not Principal.isAnonymous(caller);
        let entry : ?HashMap.HashMap<Principal, [Types.Message]> = conversationStore.get(caller);
        var exists = Option.isSome(?entry);
        switch (entry) {
            case (?exists) {
                conversationStore.delete(caller)
            };
            case (null) {}
        }
    };

    // ip
    private var ipStore : HashMap.HashMap<Text, Types.IPAddress> = HashMap.HashMap<Text, Types.IPAddress>(0, Text.equal, Text.hash);

    stable var bannedIPs : [Text] = [];

    public shared ({ caller }) func getIpAddress(postId : Text) : async Result.Result<Text, Types.Error> {
        assert Utils.isAdmin(caller);
        let postEntry : ?Types.IPAddress = ipStore.get(postId);
        let postxists = Option.isSome(?postEntry);
        switch (postEntry) {
            case (?postxists) {
                #ok(postxists.ipAddress)
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared ({ caller }) func getIPAddresses() : async Result.Result<[Types.IPAddress], Types.Error> {
        assert Utils.isAdmin(caller);
        let entries : Iter.Iter<(Text, Types.IPAddress)> = ipStore.entries();
        let iter : Iter.Iter<Types.IPAddress> = Iter.map(entries, func((key : Text, value : Types.IPAddress)) : Types.IPAddress { value });
        #ok(Iter.toArray(iter))
    };

    //     postId : Text;
    //     ipAddress : Text
    // }
    public shared ({ caller }) func banIP(postId : Text) : async Result.Result<Text, Types.Error> {
        assert Utils.isAdmin(caller);
        let ipEntry : ?Types.IPAddress = ipStore.get(postId);
        let ipExists = Option.isSome(?ipEntry);
        switch (ipEntry) {
            case (?ipExists) {
                let bannedBuffer : Buffer.Buffer<Text> = Buffer.fromArray<Text>(bannedIPs);
                let bannedBufferMatchBuffer : Buffer.Buffer<Text> = Buffer.fromArray(Array.filter(bannedIPs, func(v : Text) : Bool { v == ipExists.ipAddress }));
                if (bannedBufferMatchBuffer.size() > 0) {
                    return #err(#AlreadyReported)
                } else {
                    bannedBuffer.add(ipExists.ipAddress);
                    bannedIPs := Buffer.toArray(bannedBuffer) : [Text];
                    return #ok(ipExists.ipAddress)
                }
            };
            case (null) { #err(#NotFound) }
        }
    };

    public shared query ({ caller }) func getBannedIps() : async [Text] {
        assert Utils.isAdmin(caller);
        return bannedIPs
    };

    public shared ({ caller }) func clearBannedIps() {
        assert Utils.isAdmin(caller);
        bannedIPs := []
    };

    // polls
    public shared ({ caller }) func submitPollResponse(postId : Text, choice : Types.PollChoice) : async Result.Result<Types.Post, Types.Error> {
        assert not Principal.isAnonymous(caller);
        let post : ?Types.Post = postStore.get(postId);
        let exists = Option.isSome(?post);
        switch (post) {
            case (?exists) {
                let newChoiceBuffer : Buffer.Buffer<Types.PollChoice> = Buffer.fromArray([]);
                for (choiceOption in Iter.fromArray(exists.poll.choices)) {
                    // choice match
                    if (choiceOption.choice == choice.choice) {
                        var voterBuffer : Buffer.Buffer<Text> = Buffer.fromArray<Text>(choiceOption.voters : [Text]);
                        let voterBufferMatch : Buffer.Buffer<Text> = Buffer.fromArray(Array.filter(choiceOption.voters, func(v : Text) : Bool { v == Principal.toText(caller) }));
                        // alredy voted
                        if (voterBufferMatch.size() > 0) {
                            return #err(#AlreadyVoted)
                        } // set vote
                        else {
                            // let updatedBuffer : Buffer.Buffer<Principal> = voterBuffer.add(caller);

                            // verify Seachad
                            if (exists.poll.participants == "seachad") {
                                var verifiedSeachad = false;
                                let userEntry : ?Types.User = userStore.get(caller);
                                let userExists = Option.isSome(?userEntry);
                                switch (userEntry) {
                                    case (?userExists) {
                                        for (badge in Iter.fromArray(userExists.badges)) {
                                            if (badge == "SeaChad") {
                                                verifiedSeachad := true
                                            }
                                        };
                                        if (verifiedSeachad == false) {
                                            return #err(#NotFound)
                                        }
                                    };
                                    case (null) { return #err(#NotFound) }
                                }
                            };
                            var voterBufferNew : Buffer.Buffer<Text> = Buffer.fromArray(choiceOption.voters);
                            voterBufferNew.add(Principal.toText(caller));
                            let newChoiceOption = {
                                choice = choiceOption.choice;
                                voters = Buffer.toArray(voterBufferNew)
                            };
                            newChoiceBuffer.add(newChoiceOption)
                        }
                    } //  no choice match
                    else {
                        // remove caller if vote changed from one choice to another
                        let callerFilteredBuffer : Buffer.Buffer<Text> = Buffer.fromArray(Array.filter(choiceOption.voters, func(v : Text) : Bool { v != Principal.toText(caller) }));
                        let newChoiceOption = {
                            choice = choiceOption.choice;
                            voters = Buffer.toArray(callerFilteredBuffer)
                        };
                        newChoiceBuffer.add(newChoiceOption)
                    }
                };
                let updatedPoll = {
                    question = exists.poll.question;
                    choices = Buffer.toArray(newChoiceBuffer);
                    participants = exists.poll.participants
                };
                var updatedPost = {
                    id = exists.id;
                    threadId = exists.threadId;
                    boardId = exists.boardId;
                    subject = exists.subject;
                    body = exists.body;
                    timestamp = exists.timestamp;
                    bumpTimestamp = exists.bumpTimestamp;
                    web3 = exists.web3;
                    poster = exists.poster;
                    reports = exists.reports;
                    properties = exists.properties;
                    threadDetails = exists.threadDetails;
                    file = exists.file;
                    gate = exists.gate;
                    poll = updatedPoll
                } : Types.Post;
                postStore.put(exists.id, updatedPost);
                return #ok(updatedPost)
            };
            case (null) { #err(#NotFound) }
        }
    };

    // canister operations
    public query func getBackendCycleBalance() : async Nat { Cycles.balance() };

    public func getFrontendCycleBalance() : async Nat {
        let statusCanister = actor ("e3mmv-5qaaa-aaaah-aadma-cai") : actor {
            canister_status : ({ canister_id : Principal }) -> async Types.Canister_Status
        };
        let canister_id = await getFrontendCanisterId() : async Principal;
        let canisterStatus = await statusCanister.canister_status({
            canister_id : Principal
        });
        return canisterStatus.cycles
    };

    public func acceptCycles() : async () {
        let available = Cycles.available();
        let accepted = Cycles.accept(available);
        assert (accepted == available)
    };

    public func getBackendCanisterId() : async Principal {
        return Principal.fromActor(Seachan)
    };

    public func getFrontendCanisterId() : async Principal {

        // prod
        if (Principal.fromActor(Seachan) == Principal.fromText("y26ux-qiaaa-aaaag-aap3q-cai")) {
            return Principal.fromText("sfjch-siaaa-aaaak-qarnq-cai")
        }
        // staging
        else {
            return Principal.fromText("g2r2f-uiaaa-aaaak-qaszq-cai")
        }
    };

}

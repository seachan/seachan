import Buffer "mo:base/Buffer";
import Hash "mo:base/Hash";
import Nat32 "mo:base/Nat32";
import Principal "mo:base/Principal";
import Trie "mo:base/Trie";
import HashMap "mo:base/HashMap";

module Types {

    public type BoardCreate = {
        id : Text;
        name : Text;
        properties : BoardProperties;
        gate : Gate
    };

    type BoardProperties = {
        unListed : Bool;
        flags : Bool;
        text : Bool;
        anonymous : Bool
    };

    public type Gate = {
        token : Text;
        amount : Nat32;
        nft : Text
    };

    public type Web3 = {
        icpBalance : Float;
        icpReceived : Float;
        acceptIcp : Bool
    };

    public type Web3In = {
        icpBalance : Float;
        acceptIcp : Bool
    };

    public type Poster = {
        posterId : PosterId;
        badges : [Text];
        directMessage : Bool;
        flag : Text;
        country : Text
    };

    public type PosterPublic = {
        posterId : PosterId;
        badges : [Text];
        directMessage : Bool;
        flag : Text;
        country : Text
    };

    public type PosterIn = {
        posterId : PosterId;
        badges : [Text];
        directMessage : Bool;
        ipAddress : Text;
        flag : Text;
        country : Text
    };

    public type Properties = {
        op : Bool;
        stickied : Bool;
        locked : Bool;
        flagged : Bool;
        saged : Bool
    };

    public type IPAddress = {
        postId : Text;
        ipAddress : Text
    };

    public type Poll = {
        question : Text;
        choices : [PollChoice];
        participants : Text
    };

    public type PollChoice = {
        choice : Text;
        voters : [Text]
    };

    public type Reports = {
        seaChadPrincipals : [Principal];
        count : Nat32
    };

    public type Board = {
        id : Text;
        name : Text;
        ownerPrincipal : Text;
        timestamp : Int;
        bumpTimestamp : Int;
        postCount : Nat32;
        threadCount : Nat32;
        fileCount : Nat32;
        fileSize : Nat32;
        properties : BoardProperties;
        gate : Gate;
        views : Nat32
    };

    public type ThreadDetails = {
        postIds : [Text];
        views : Nat32;
        fileCount : Nat32;
        fileSize : Nat32;
        uniquePosters : [PosterId]
    };

    public type Post = {
        id : Text;
        boardId : Text;
        threadId : Text;
        subject : Text;
        body : Text;
        timestamp : Int;
        bumpTimestamp : Int;
        web3 : Web3;
        poster : Poster;
        reports : Reports;
        properties : Properties;
        file : PostFile;
        gate : Gate;
        threadDetails : ThreadDetails;
        poll : Poll
    };

    public type ImportedPost = {
        id : Text;
        boardId : Text;
        threadId : Text;
        subject : Text;
        body : Text;
        timestamp : Int;
        bumpTimestamp : Int;
        web3 : Web3;
        poster : Poster;
        reports : Reports;
        properties : Properties;
        file : PostFile;
        gate : Gate;
        threadDetails : ThreadDetails;
        poll : Poll
    };

    public type PostCreate = {
        id : Text;
        threadId : Text;
        boardId : Text;
        subject : Text;
        body : Text;
        web3 : Web3In;
        poster : PosterIn;
        properties : Properties;
        file : PostFile;
        gate : Gate;
        poll : Poll
    };

    public type PostFile = {
        path : Text;
        nsfw : Bool;
        format : Text;
        name : Text;
        size : Nat32
    };

    public type User = {
        principal : Principal;
        principalProvider : Text;
        usernames : [Text];
        badges : [Text];
        timestamp : Int;
        navbarBoards : [Text];
        preferences : UserPreferences;
        savedPosts : {
            watchedThreads : [Text];
            hiddenPosts : [Text];
            minimizedPosts : [Text]
        }
    };

    private type UserPreferences = {
        showNsfw : Bool;
        showFlagged : Bool;
        boardView : Text;
        threadView : Text;
        directMessaging : Bool
    };

    public type NftCollection = {
        name : Text;
        canisterId : Text;
        standard : Text;
        tokens : [NFTDetails];
        icon : Text;
        description : Text
    };

    public type NFTDetails = {
        index : Nat32;
        url : Text
    };

    public type PosterId = {
        #Admin : Text;
        #Anon : Text;
        #Principal : Text;
        #Username : Text
    };

    public type Error = {
        #NotFound;
        #AlreadyExists;
        #NotAuthorized;
        #AlreadyReported;
        #Error;
        #AlreadyVoted
    };

    public type CreateError = {
        #AlreadyExists
    };

    public type CreatePostError = {
        #BoardNotFound
    };

    public type GetPostError = {
        #PostNotFound
    };

    public type CreateMessageError = {
        #UserNotFound
    };

    public type ReportType = {
        #LowQuality;
        #OffTopic;
        #Spam;
        #Scam;
        #Illegal
    };

    public type TagType = {
        #Based;
        #Cringe
    };

    public type MessageCreate = {
        fromPosterId : PosterId;
        toPrincipal : Principal;
        toPosterId : PosterId;
        postId : Text;
        subject : Text;
        body : Text
    };
    public type Message = {
        subject : Text;
        body : Text;
        timestamp : Int
    };

    // public type Conversations = {
    //     conversation : [Conversation]
    // };

    public type Conversation = {
        fromPrincipal : Principal;
        fromPosterId : PosterId;
        toPrincipal : Principal;
        toPosterId : PosterId;
        postId : Text;
        messages : HashMap.HashMap<Principal, [Types.Message]>
    };

    public type Definite_Canister_Settings = {
        freezing_threshold : Nat;
        controllers : [Principal];
        memory_allocation : Nat;
        compute_allocation : Nat
    };

    public type Canister_Status = {
        status : { #stopped; #stopping; #running };
        memory_size : Nat;
        cycles : Nat;
        settings : Definite_Canister_Settings;
        module_hash : ?[Nat8]
    };

    // type Badges = {
    //     #CycleContributor : Text;
    //     #Admin : Text;
    // };

}

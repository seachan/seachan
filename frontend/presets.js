export const presets = [
  {
    name: "dark",
    colors: {
      text: "#FFFFFF",
      background: "#112031",
      navbar: "#27445C", //33495E
      postBg: "#33495E", // #4B616F; //#413F42 // #4B616F
      danger: "#FF7F7F",
      subject: "#FFCE42",
      sage: "#90EE90",
      highlight: "#336699",
      green: "#66FF00",
      quote: "#F7A579",
    },
  },
  {
    name: "light",
    colors: {
      text: "black",
      background: "#fff1d9",
      navbar: "#fca",
      postBg: "#FFFFFF",
      danger: "red",
      subject: "red",
      sage: "#3A5F0B",
      highlight: "#F8C379",
      green: "#008000",
      quote: "#2E2EFF",
    },
  },
]

import { writable } from "svelte/store"

export const userCountStore = writable(0)

export const isFetchingUserCount = writable(false)
export function fetchUserCount(actor) {
  isFetchingUserCount.set(true)
  actor.getUserCount().then((userCountFetched) => {
    userCountStore.set(userCountFetched)
    isFetchingUserCount.set(false)
  })
}

import { writable, derived } from "svelte/store"

export const isFetchingBoards = writable(false)
export const boardsInStore = writable([])
export const boardsSortedStore = writable([])
export const boardCountStore = writable(0)
export const postCountStore = writable(0)
export const threadCountStore = writable(0)
export const sortBy = writable({ col: "bumpTimestamp", ascending: false })
export const filter = writable({ status: "All" })

export function fetchBoards(actor) {
  isFetchingBoards.set(true)
  // check here

  actor.getListedBoards().then((fetchedBoardsResult) => {
    let boardsResult = fetchedBoardsResult["ok"]
    boardCountStore.set(boardsResult.length)
    if (boardsResult.length > 0) {
      let postCount = boardsResult
        .map((i) => Number(i?.postCount))
        .reduce((a, b) => a + b)
      let threadCount = boardsResult
        .map((i) => Number(i?.threadCount))
        .reduce((a, b) => a + b)
      boardsInStore.set(boardsResult)
      postCountStore.set(postCount)
      threadCountStore.set(threadCount)
    }
    isFetchingBoards.set(false)
  })
}

export const boardsOutStore = derived(
  [boardsInStore, sortBy],
  ([$boardsInStore, $sortBy]) => {
    if (boardsInStore) {
      let sortModifier = $sortBy.ascending ? 1 : -1
      let sortField = $sortBy.col.split(".")
      if (sortField.length == 1) {
        return $boardsInStore.sort((a, b) =>
          a[$sortBy.col] > b[$sortBy.col]
            ? 1 * sortModifier
            : b[$sortBy.col] > a[$sortBy.col]
            ? -1 * sortModifier
            : 0,
        )
      } else if (sortField.length == 2) {
        return $boardsInStore.sort((a, b) =>
          a[sortField[0]][sortField[1]] > b[sortField[0]][sortField[1]]
            ? 1 * sortModifier
            : b[sortField[0]][sortField[1]] > a[sortField[0]][sortField[1]]
            ? -1 * sortModifier
            : 0,
        )
      } else if (sortField.length == 3) {
        return $boardsInStore.sort((a, b) =>
          a[sortField[0]][sortField[1]].length >
          b[sortField[0]][sortField[1]].length
            ? 1 * sortModifier
            : b[sortField[0]][sortField[1]].length >
              a[sortField[0]][sortField[1]].length
            ? -1 * sortModifier
            : 0,
        )
      }
    } else {
      return []
    }
  },
)

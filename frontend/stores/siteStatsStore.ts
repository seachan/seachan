import { writable } from "svelte/store"

export const isFetchingVisitCount = writable(false)
export const visitCountStore = writable(0)

export function fetchVisitCount(actor) {
  isFetchingVisitCount.set(true)
  actor.getVisitCount().then((visitCountFetched) => {
    visitCountStore.set(visitCountFetched)
    isFetchingVisitCount.set(false)
  })
}

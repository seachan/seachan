import { writable } from "svelte/store"
import { USE_FAKE_DATA, UNDER_MAINTENANCE } from "../constants"

export const useFakeData = writable(USE_FAKE_DATA)
export const underMaintenance = writable(UNDER_MAINTENANCE)

import { get, writable, derived } from "svelte/store"
import { generateData } from "../data/generateData"
import {
  boardsInStore,
  boardCountStore,
  postCountStore,
  threadCountStore,
  boardsOutStore,
} from "../stores/boardsStore"
import { boardStore } from "../stores/boardStore"
import {
  boardThreadsRootStore,
  latestThreadStore,
  postsOutStore,
  boardPostsStore,
  threadRootStore,
  isFetchingPosts,
  threadNotFound,
} from "../stores/postsStore"

export const isFetchingFakeData = writable(false)
export function fetchFakeData() {
  isFetchingFakeData.set(true)
  var generatedData = generateData()
  let boards = generatedData.boards
  let posts = generatedData.posts
  // latest threads
  latestThreadStore.set(
    posts
      .filter(function (p) {
        return p.properties.op == true && !p.file.nsfw
      })
      .sort((a, b) => Number(b.timestamp) - Number(a.timestamp))
      .slice(0, 5),
  )
  let users = generatedData.users
  boardCountStore.set(boards.length)
  if (posts.length > 0) {
    boardPostsStore.set(posts)
    let postCount = boards
      .map((i) => Number(i?.postCount))
      .reduce((a, b) => a + b)
    let threadCount = boards
      .map((i) => Number(i?.threadCount))
      .reduce((a, b) => a + b)
    boardsInStore.set(boards)
    postCountStore.set(postCount)
    threadCountStore.set(threadCount)
  }
  isFetchingFakeData.set(false)
}

export const isFetchingBoard = writable(false)
export function fetchFakeBoard(boardId) {
  boardStore.set(get(boardsOutStore).find((board) => board.id === boardId))
}

export function fetchFakeBoardThreads(boardId) {
  let posts = get(postsOutStore)
  let boardThreads = posts.filter(
    (post) => post.boardId === boardId && post.properties.op == true,
  )
  boardThreadsRootStore.set(boardThreads)
}

export function fetchFakeThread(boardId, threadId) {
  isFetchingPosts.set(true)
  threadRootStore.set(
    get(postsOutStore).filter(
      (post) => post.boardId === boardId && post.threadId == threadId,
    ),
  )
  isFetchingPosts.set(false)
}

import { writable, derived } from "svelte/store"
import type { Writable } from "svelte/store"
import type { Board } from "../../src/declarations/backend/backend.did"

export const boardStore: Writable<Board> = writable()
export var boardNotFound = writable(false)

export const isFetchingBoard = writable(false)
export function fetchBoard(actor, boardId) {
  isFetchingBoard.set(true)
  actor.getBoard(boardId).then((fetchedBoardResult) => {
    if (fetchedBoardResult["ok"]) {
      boardNotFound.set(false)
      boardStore.set(fetchedBoardResult["ok"])
      actor.incrementBoardViewCount(fetchedBoardResult["ok"].id)
    } else {
      boardNotFound.set(true)
    }
    isFetchingBoard.set(false)
  })
}

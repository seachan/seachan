import { writable } from "svelte/store"
import type { Writable } from "svelte/store"
import type { User } from "../../src/declarations/backend/backend.did"
import { getAllUserNFTs } from "@psychedelic/dab-js"
import { useConnect } from "@connect2ic/svelte"

export const userStore: Writable<User> = writable()
export const userNFTs = writable([])

export const isFetchingUser = writable(false)
export function fetchUser(actor, activeProvider) {
  isFetchingUser.set(true)
  actor.getOrCreateUser(activeProvider.meta.id).then((userFetched) => {
    userStore.set(userFetched["ok"])
    isFetchingUser.set(false)
  })
}

export const isFetchingUserNFTs = writable(false)
export async function fetchUserNFTs(principal) {
  isFetchingUserNFTs.set(true)
  const nftCollections = await getAllUserNFTs({
    user: principal,
  })
  userNFTs.set(nftCollections)
  isFetchingUserNFTs.set(false)
}

import { writable, derived } from "svelte/store"
import { sort } from "../utils/sort"
import type { Post } from "src/declarations/backend/backend.did"
import { Principal } from "@dfinity/principal"

let nullPost: Post = {
  id: "",
  bumpTimestamp: 0n,
  subject: "",
  body: "",
  file: {
    name: "",
    nsfw: false,
    path: "",
    size: 0,
    format: "",
  },
  gate: { nft: "", token: "", amount: 0 },
  poll: {
    participants: "",
    question: "",
    choices: [],
  },
  web3: { icpReceived: 0, acceptIcp: false, icpBalance: 0 },
  threadDetails: {
    postIds: [],
    views: 0,
    fileCount: 0,
    fileSize: 0,
    uniquePosters: [],
  },
  properties: {
    op: false,
    locked: false,
    saged: false,
    stickied: false,
    flagged: false,
  },
  boardId: "",
  timestamp: 0n,
  reports: { seaChadPrincipals: [], count: 0 },
  threadId: "",
  poster: {
    posterId: { Anon: "" },
    country: "",
    // ownerPrincipal: "2vxsx-fae",
    flag: "",
    badges: [],
    directMessage: false,
    // ipAddress: "",
  },
}

// string search
export const stringFilter = writable({ status: "All" })
export const boolFilter = writable({ status: [] })
export const displayFlagged = writable(false)
export const isFetchingLatestThreads = writable(false)
export const boardPostsStore = writable([])
export const highlightedPosts = writable([])
export const boardThreadsRootStore = writable([])
export const replyingToStore = writable<Post>(nullPost)
export const sortBy = writable({
  col: "bumpTimestamp",
  ascending: true,
})
// export const threadSortBy = writable({ col: "timestamp", ascending: true })
export const latestThreadStore = writable([])
export const isFetchingPosts = writable(false)
export const fileCountStore = writable(0)
export const fileSizeStore = writable(0)
export var threadNotFound = writable(false)
export const postsOutStore = derived(
  [boardPostsStore, sortBy],
  ([$filteredThreadStore, $sortBy]) => {
    let sortModifier = $sortBy.ascending ? 1 : -1
    let sortField = $sortBy.col.split(".")
    if (sortField.length == 1) {
      return $filteredThreadStore.sort((a, b) =>
        a[$sortBy.col] > b[$sortBy.col]
          ? 1 * sortModifier
          : b[$sortBy.col] > a[$sortBy.col]
          ? -1 * sortModifier
          : 0,
      )
    } else if (sortField.length == 2) {
      return $filteredThreadStore.sort((a, b) =>
        a[sortField[0]][sortField[1]] > b[sortField[0]][sortField[1]]
          ? 1 * sortModifier
          : b[sortField[0]][sortField[1]] > a[sortField[0]][sortField[1]]
          ? -1 * sortModifier
          : 0,
      )
    } else if (sortField.length == 3) {
      return $filteredThreadStore.sort((a, b) =>
        a[sortField[0]][sortField[1]].length >
        b[sortField[0]][sortField[1]].length
          ? 1 * sortModifier
          : b[sortField[0]][sortField[1]].length >
            a[sortField[0]][sortField[1]].length
          ? -1 * sortModifier
          : 0,
      )
    }
  },
)

export function fetchPosts(actor) {
  isFetchingPosts.set(true)
  actor.getPosts().then((postResult) => {
    let posts = postResult["ok"]
    // postStore.set(posts)
    fileCountStore.set(
      posts.filter(function (p) {
        return p.file.path != ""
      }).length,
    )
    fileSizeStore.set(
      posts.length > 0
        ? posts.map((i) => Number(i?.file?.size)).reduce((a, b) => a + b)
        : 0,
    )
    isFetchingPosts.set(false)
  })
}

export const threadRootStore = writable([])

// fetch, filter, sort thread
export function fetchThread(actor, boardId, threadId) {
  // threadRootStore.set([])
  isFetchingPosts.set(true)
  actor.getThread(boardId, threadId).then((threadsResult) => {
    if (threadsResult["ok"]) {
      if (threadsResult["ok"].length == 0) {
        threadNotFound.set(true)
      }
      threadRootStore.set(threadsResult["ok"])
      actor.incrementThreadViewCount(threadId)
    }
    isFetchingPosts.set(false)
  })
}

// fetch, filter, sort board threads
export const threadStore = derived(
  [threadRootStore, stringFilter, boolFilter, sortBy, displayFlagged],
  ([
    $threadRootStore,
    $stringFilter,
    $boolFilter,
    $sortBy,
    $displayFlagged,
  ]) => {
    let posts = []
    if ($displayFlagged) {
      posts = $threadRootStore
    } else {
      posts = $threadRootStore.filter(function (p) {
        return p.properties.flagged == false
      })
    }
    if ($boolFilter.status.length > 0) {
      $boolFilter.status.forEach((filterObj) => {
        // filter
        if (filterObj.value) {
          let filterField = filterObj.field.split(".")
          if (
            ["web3.icpReceived", "web3.icpBalance"].includes(filterObj.field)
          ) {
            posts = posts.filter((x) => x[filterField[0]][filterField[1]] != 0)
          }
          if (
            [
              "web3.acceptIcp",
              "poster.directMessage",
              "properties.flagged",
              "properties.saged",
              "file.nsfw",
            ].includes(filterObj.field)
          ) {
            posts = posts.filter(
              (x) => x[filterField[0]][filterField[1]] == true,
            )
          }
          if (
            ["reports.seaChadPrincipals", "poster.badges"].includes(
              filterObj.field,
            )
          ) {
            posts = posts.filter(
              (x) => x[filterField[0]][filterField[1]].length > 0,
            )
          }
        }
      })
    }
    // string search
    let stringFilteredPosts =
      $stringFilter.status == "All"
        ? posts
        : posts.filter(
            (x) =>
              x.subject
                .toLowerCase()
                .includes($stringFilter.status.toLowerCase()) ||
              x.body.toLowerCase().includes($stringFilter.status.toLowerCase()),
          )
    let sortedThread = sort(stringFilteredPosts, $sortBy)
    return sortedThread
  },
)

export function fetchLatestsThreads(actor) {
  isFetchingLatestThreads.set(true)
  actor.getThreads().then((threadResult) => {
    latestThreadStore.set(
      threadResult["ok"]
        .sort((a, b) => Number(b.timestamp) - Number(a.timestamp))
        .slice(0, 5),
    )
    isFetchingLatestThreads.set(false)
  })
}

export function fetchBoardThreads(actor, boardId) {
  isFetchingPosts.set(true)
  actor.getBoardThreads(boardId).then((threadsResult) => {
    if (threadsResult["ok"].length > 0) {
      boardThreadsRootStore.set(threadsResult["ok"])
    } else {
      boardThreadsRootStore.set([])
    }
    isFetchingPosts.set(false)
  })
}

// fetch, filter, sort board threads
export const boardThreadsStore = derived(
  [boardThreadsRootStore, stringFilter, boolFilter, sortBy, displayFlagged],
  ([
    $boardThreadsStore,
    $stringFilter,
    $boolFilter,
    $sortBy,
    $displayFlagged,
  ]) => {
    let threads = []
    if ($displayFlagged) {
      threads = $boardThreadsStore
    } else {
      threads = $boardThreadsStore.filter(function (p) {
        return p.properties.flagged == false
      })
    }
    if ($boolFilter.status.length > 0) {
      $boolFilter.status.forEach((filterObj) => {
        // filter
        if (filterObj.value) {
          let filterField = filterObj.field.split(".")
          if (
            ["web3.icpReceived", "web3.icpBalance"].includes(filterObj.field)
          ) {
            threads = threads.filter(
              (x) => x[filterField[0]][filterField[1]] != 0,
            )
          }
          if (
            [
              "web3.acceptIcp",
              "poster.directMessage",
              "properties.flagged",
              "properties.saged",
              "file.nsfw",
            ].includes(filterObj.field)
          ) {
            threads = threads.filter(
              (x) => x[filterField[0]][filterField[1]] == true,
            )
          }
          if (
            ["poster.flag", "gate.token", "gate.nft"].includes(filterObj.field)
          ) {
            threads = threads.filter(
              (x) => x[filterField[0]][filterField[1]] != "",
            )
          }
          if (
            ["reports.seaChadPrincipals", "poster.badges"].includes(
              filterObj.field,
            )
          ) {
            threads = threads.filter(
              (x) => x[filterField[0]][filterField[1]].length > 0,
            )
          }
        }
      })
    }
    // string search
    let stringFilteredThreads =
      $stringFilter.status == "All"
        ? threads
        : threads.filter(
            (x) =>
              x.subject
                .toLowerCase()
                .includes($stringFilter.status.toLowerCase()) ||
              x.body.toLowerCase().includes($stringFilter.status.toLowerCase()),
          )
    let sticky = stringFilteredThreads.filter(function (p) {
      return p.properties.stickied == true
    })
    let nonSticky = stringFilteredThreads.filter(function (p) {
      return p.properties.stickied == false
    })
    let stickySorted = sticky.concat(sort(nonSticky, $sortBy))
    return stickySorted
  },
)

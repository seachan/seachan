import { faker } from "@faker-js/faker"
import { Principal } from "@dfinity/principal"
import type { Board } from "src/declarations/backend/backend.did"
import { generateAnonId } from "../utils/generateAnonId"
import { ANON_REPORT_LIMIT } from "../constants"

/**
 * @param {number} min
 * @param {number} max
 */
function randomIntFromInterval(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

function weightedRandom(items, weights) {
  var i
  for (i = 0; i < weights.length; i++) weights[i] += weights[i - 1] || 0
  var random = Math.random() * weights[weights.length - 1]
  for (i = 0; i < weights.length; i++) if (weights[i] > random) break
  return items[i]
}

function randomBigIntStringFromInterval() {
  let current = Date.now() * 1_000_000
  let offset = (
    current - randomIntFromInterval(0, 10_000_000_000_000)
  ).toString()
  return offset
}

function makeRandomString(length) {
  var result = ""
  var characters = "abcdefghijklmnopqrstuvwxyz0123456789"
  var charactersLength = characters.length
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }

  return result
}

function capitalizeFirstLetter(string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

function generatePrincipal() {
  var principalCombined = makeRandomString(54)
  var principalDashed = principalCombined.match(/.{1,5}/g).join("-")
  return principalDashed
}

function generateSeaChadPrincipals() {
  // reporter principals
  var addSeaChadPrincipals = Math.random() < 0.5
  if (addSeaChadPrincipals) {
    var seaChadPrincipals = new Array(randomIntFromInterval(0, 9))
    for (let i = 0; i < seaChadPrincipals.length; i++) {
      seaChadPrincipals[i] = "2vxsx-fae"
    }
  } else {
    seaChadPrincipals = []
  }
  return seaChadPrincipals
}

function generateFileData(boardStats, threadId, op) {
  // file info
  var hasFile = op ? true : faker.datatype.boolean()
  if (hasFile) {
    var fileTypeArray = ["audio", "document", "image", "video", "zip"]
    // const weights = [0.05, 0.05, 0.8, 0.05, 0.05]
    const weights = [0.25, 0.25, 0.25, 0.25, 0.25]
    var type = weightedRandom(fileTypeArray, weights)
    var nsfw = false
    if (type == "audio") {
      var path =
        "https://dweb.link/ipfs/bafkreignfj44dtmmc7qgy3qh6twfvi4uvsi7z34xo5ulwrykrfiy34gd5a"
      var extension = ".mp3"
      var name = "Vocaroo 19FUI2vM4VmL.mp3"
    } else if (type == "image") {
      var path = faker.image.nature(640, 480, true)
      var extension = ".jpg"
      var name = faker.system.commonFileName("jpg")
      nsfw = threadId == 0 ? false : Math.random() < 0.25
    } else if (type == "document") {
      var path =
        "https://dweb.link/ipfs/bafybeid7qsdllyz3mnie675b7tks7jqqth6nwp5nl3qgulksw4fnmb4owq"
      var extension = ".pdf"
      var name =
        "A History of Central Banking and the Enslavement of Mankind.pdf"
    } else if (type == "video") {
      var path =
        "https://dweb.link/ipfs/bafybeifqrsqltwmd7phu7rlaksksk2qoo7wmoaeqi6u64ijfvz2c4hvdly"
      var extension = ".webm"
      var name = "ICP6sound.webm"
      nsfw = Math.random() < 0.25
    } else if (type == "zip") {
      var path = "https://archlinux.org/packages/extra/x86_64/zip/"
      var extension = ".zip"
      var name = "zip.zip"
    } else {
      var path = ""
    }
    var size = parseInt(faker.random.numeric(4))
    return {
      path: path,
      nsfw: nsfw,
      format: type,
      name: name,
      size: size,
    }
  } else {
    return {
      path: "",
      nsfw: false,
      format: "",
      name: "",
      size: 0,
    }
  }
}

function generatePosterId() {
  var siteAnonId = generateAnonId()
  var posterType = faker.helpers.arrayElement(["Anon", "Username", "Principal"])
  var posterId
  switch (posterType) {
    case "Anon":
      posterId = siteAnonId
      break
    case "Principal":
      posterId = "2vxsx-fae"
      break
    case "Username":
      posterId = faker.internet.userName()
      break
    default:
      posterId = ""
  }
  return { [posterType]: posterId }
}

// function dedupBoards(boards) {
//   const uniqueIds = []
//   const unique = boards.filter((element) => {
//     const isDuplicate = uniqueIds.includes(element.id)
//     if (!isDuplicate) {
//       uniqueIds.push(element.id)
//       return true
//     }
//     return false
//   })
//   return unique
// }

function generateBoards() {
  let boards = new Array(randomIntFromInterval(5, 20))

  for (let i = 0; i < boards.length; i++) {
    var name = faker.lorem.words(randomIntFromInterval(1, 2))
    var nameCapitalized = name
      .split(" ")
      .map((nameWord) => {
        return capitalizeFirstLetter(nameWord)
      })
      .join(" ")
    var id = name
      .split(" ")
      .map((nameWord) => {
        return nameWord[0]
      })
      .join("")
    // gate
    // var gated = Math.random() < 0.25
    var gated = false
    if (gated) {
      var gateType = faker.helpers.arrayElement(["tokens"])
      if (gateType == "tokens") {
        var token = faker.helpers.arrayElement(["ICP"]) //  "XTC", "WICP"
        var amount = randomIntFromInterval(0, 3)
      }
      // else if (gateType == "nft") {
      //   var token = faker.helpers.arrayElement(["motoko"])
      //   var amount = 0
      // }
    } else {
      var token = ""
      var amount = 0
    }
    // stats
    let postCount = randomIntFromInterval(1, 1000)
    // created board
    var createdBoard = {
      id: id, // /qt/
      name: nameCapitalized, // Quia Tenetur
      ownerPrincipal: "", //Principal.anonymous(),
      timestamp: Date.now(),
      bumpTimestamp: Date.now(),
      postCount: postCount,
      threadCount: randomIntFromInterval(1, postCount),
      fileCount: randomIntFromInterval(1, postCount),
      fileSize: randomIntFromInterval(1, postCount),
      properties: {
        unListed: false,
        flags: faker.datatype.boolean(),
        text: Math.random() < 0.1,
        anonymous: faker.datatype.boolean(),
      },
      gate: {
        token: token,
        nft: "",
        amount: amount,
      },
      views: randomIntFromInterval(1, 1000),
    }
    boards[i] = createdBoard
  }

  // dedup on abbreviation
  const uniqueIds = []
  boards.filter((element) => {
    const isDuplicate = uniqueIds.includes(element.id)
    if (!isDuplicate) {
      uniqueIds.push(element.id)
    }
  })

  return boards
}

function generateThreads(boards) {
  var posts = []
  boards.forEach((board) => {
    let threadCount = randomIntFromInterval(5, 25)
    let threads = new Array(threadCount)
    let idCounter = 0
    for (let i = 0; i < threads.length; i++) {
      var threadId = board.id + "/" + i
      // web3
      var hasWeb3 = Math.random() < 0.5
      if (hasWeb3) {
        var acceptIcp = i == 0 ? false : Math.random() < 0.5
        var icpReceived = acceptIcp ? randomIntFromInterval(0, 100) : 0
        var icpBalance = randomIntFromInterval(0, 100)
      } else {
        var acceptIcp = false
        var icpReceived = 0
        var icpBalance = 0
      }
      // stats
      let posterId = generatePosterId()
      // var posterIds = [poster.posterId]
      // reporter principals
      var seaChadPrincipals = generateSeaChadPrincipals()
      // replies
      let replyCount = randomIntFromInterval(5, 50)
      idCounter = i
      var replyData = generateReplies(
        replyCount,
        threadId,
        threadCount,
        board,
        posterId,
        idCounter,
      )
      posts = replyData.replies.concat(posts)
      // gate
      var gated = Math.random() < 0.25
      if (gated) {
        var gateType = faker.helpers.arrayElement(["tokens"])
        if (gateType == "tokens") {
          var token = faker.helpers.arrayElement(["ICP"]) //  "XTC", "WICP"
          var amount = randomIntFromInterval(1, 3)
        }
        // else if (gateType == "nft") {
        //   var token = faker.helpers.arrayElement(["motoko"])
        //   var amount = 0
        // }
      } else {
        var token = ""
        var amount = 0
      }
      // poll
      let poll = {
        question: "",
        choices: [],
        participants: "",
      }
      var hasPoll = Math.random() < 0.25
      if (hasPoll) {
        let choiceCount = randomIntFromInterval(2, 10)
        let choiceArray = []
        for (let i = 0; i < choiceCount; i++) {
          let voteCount = randomIntFromInterval(0, 100)
          let voteArray = []
          for (let j = 0; j < voteCount; j++) {
            voteArray.push("2vxsx-fae")
          }
          choiceArray.push({
            choice: faker.lorem.sentence(),
            voters: voteArray,
          })
        }
        poll = {
          question: faker.lorem.sentence().replace(".", "?"),
          choices: choiceArray,
          participants: faker.helpers.arrayElement([
            "authenticated",
            "seachad",
          ]),
        }
      }
      // created thread
      var createdThread = {
        id: board.id + "/" + idCounter,
        boardId: board.id,
        threadId: threadId,
        subject: faker.lorem.sentence(),
        body: faker.lorem.paragraph(), //">green text test\n>>40", //  ">wsrwrwerwrg\n>wsrwrwerwrg\nadgswrgwrgwrg\ngoogle.com\n>>40" //faker.lorem.paragraph()
        //  faker.helpers.mustache('I found {{count}} instances of "{{word}}".', {
        //   count: () => `${faker.datatype.number()}`,
        //   word: "this word",
        // })

        timestamp: 1659990712489002496,
        bumpTimestamp: 1659990712489002496, // "1659990712489002496" // Date.now()
        // views: randomIntFromInterval(0, 1000),
        web3: {
          icpBalance: icpBalance,
          acceptIcp: acceptIcp,
          icpReceived: icpReceived,
        },
        poster: {
          posterId: posterId,
          // ownerPrincipal: Principal.anonymous(),
          badges: [faker.helpers.arrayElement(["SeaChad", "CycleSender", ""])],
          directMessage: faker.datatype.boolean(),
          // ipAddress: faker.internet.ip(),
          flag: "\uD83C\uDDFA\uD83C\uDDF8",
          country: faker.address.country(),
        },
        reports: {
          seaChadPrincipals: [], //seaChadPrincipals,
          count: 0, //seaChadPrincipals.length,
        },
        properties: {
          op: true,
          stickied: i == 0 ? true : false,
          locked: i == 0 ? true : Math.random() < 0.1,
          saged: false,
          flagged: false,
        },
        threadDetails: {
          postIds: ["test", "test"],
          views: randomIntFromInterval(1, 1000),
          fileCount: randomIntFromInterval(1, 1000),
          fileSize: randomIntFromInterval(1, 1000000),
          uniquePosters: [],
        },
        gate: {
          token: token,
          nft: "",
          amount: amount,
        },
        poll: poll,
        file: generateFileData(board.stats, threadId, true),
      }
      threads[i] = createdThread
    }
    posts = posts.concat(threads)
  })
  return posts
}

function generateReplies(
  replyCount,
  threadId,
  threadCount,
  board,
  posterIdParam,
  idCounter,
) {
  var posterIds = []
  // replies
  var replies = new Array(replyCount)
  for (let i = 0; i < replyCount; i++) {
    // web3
    var acceptIcp = i == 0 ? false : Math.random() < 0.25
    var icpReceived = acceptIcp ? randomIntFromInterval(0, 100) : 0
    // poster
    var existingPoster = faker.datatype.boolean()
    let posterId = posterIdParam
    if (!existingPoster) {
      posterId = generatePosterId()
    }
    // web3
    var hasWeb3 = Math.random() < 0.25
    if (hasWeb3) {
      var acceptIcp = i == 0 ? false : Math.random() < 0.25
      var icpReceived = acceptIcp ? randomIntFromInterval(0, 100) : 0
      var icpBalance = randomIntFromInterval(0, 100)
    } else {
      var acceptIcp = false
      var icpReceived = 0
      var icpBalance = 0
    }
    // poster id
    posterIds.concat(posterId)
    let newId = i + parseInt(idCounter)
    // reporter principals
    var seaChadPrincipals = generateSeaChadPrincipals()
    // reply
    var createdReply = {
      id: board.id + "/" + newId,
      boardId: board.id,
      threadId: threadId,
      subject: "",
      body: faker.internet.url(), //faker.internet.url() //faker.lorem.paragraph() + "\n" +
      timestamp: 1659990712489002496,
      bumpTimestamp: 1659990712489002496,
      web3: {
        icpBalance: icpBalance,
        acceptIcp: acceptIcp,
        icpReceived: icpReceived,
      },
      poster: {
        posterId: posterId,
        // ownerPrincipal: Principal.anonymous(),
        badges: [faker.helpers.arrayElement(["SeaChad", "CycleSender", ""])],
        directMessage: faker.datatype.boolean(),
        // ipAddress: faker.internet.ip(),
        flag: "\uD83C\uDDFA\uD83C\uDDF8",
        country: faker.address.country(),
      },
      properties: {
        op: false,
        stickied: false,
        locked: false,
        saged: Math.random() < 0.1,
        flagged: false,
      },
      reports: {
        seaChadPrincipals: [], //seaChadPrincipals,
        count: 0, //seaChadPrincipals.length,
      },
      threadDetails: {
        postIds: [],
        views: 0,
        fileCount: 0,
        fileSize: 0,
        uniquePosters: [],
      },
      gate: {
        token: "",
        nft: "",
        amount: 0,
      },
      file: generateFileData(board.stats, threadId, false),
      poll: {
        question: "",
        choices: [],
        participants: "",
      },
    }
    replies[i] = createdReply
  }
  return { replies, posterIds }
}

function generateUsers(boards) {
  let users = new Array(randomIntFromInterval(1, 100))
  for (let i = 0; i < users.length; i++) {
    // created user
    var createdUser = {
      principal: generatePrincipal(),
      principalProvider: faker.helpers.arrayElement(["plug", "stoic"]),
      userNames: [...new Array(randomIntFromInterval(0, 20))].map(() =>
        faker.internet.userName(),
      ),
      badges: faker.helpers.arrayElement(["SeaChad", "CycleSender", ""]),
      timestamp: randomBigIntStringFromInterval(),
      minimizedPosts: [],
      hiddenPosts: [],
      navbarBoards: faker.helpers.arrayElements(boards.id),
      watchedThreads: [],
      preferences: {
        showNsfw: faker.datatype.boolean(),
        showFlagged: faker.datatype.boolean(),
      },
    }
    users[i] = createdUser
  }
  return users
}

export function generateData() {
  var boards = generateBoards()
  var posts = generateThreads(boards)
  var users = generateUsers(boards)

  return { boards, posts, users }
}

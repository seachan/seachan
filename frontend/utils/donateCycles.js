// @ts-nocheck
import { toasts } from "svelte-toasts"

export async function donateCycles(params) {
  // try {
    window.ic.plug.requestBurnXTC(params).then((result) => {
      // console.log("result",result)
      toasts.add({
        description:
          "Cycle transfer completed successfully. Thank you for the " +
          parseFloat(params.amount) +
          "T cycles. The transfer completed at height " +
          result +
          ". A CycleSender badge has been awarded to you for your donation.",
        type: "success",
      })
    })
  // } catch (error) {
  //   toasts.add({
  //     description: error,
  //     type: "error",
  //   })
  // }
}

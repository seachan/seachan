import { generateAnonId } from "./generateAnonId"

export function anonIdToThreadId(post) {
  if (Object.keys(post.poster.posterId)[0] == "Anon") {
    post.poster.posterId = {
      Anon: generateAnonId(
        Object.values(post.poster.posterId)[0] + post.boardId + post.threadId,
      ),
    }
    return post
  } else {
    return post
  }
}

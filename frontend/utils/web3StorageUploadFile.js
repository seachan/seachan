import { Web3Storage } from "web3.storage"
import { toasts } from "svelte-toasts"

export async function web3StorageUploadFile(files) {
  const apiToken = import.meta.env.VITE_WEB3_STORAGE_API
  const storage = new Web3Storage({ token: apiToken })
  try {
    const cid = await storage.put(files, {
      wrapWithDirectory: false,
      maxRetries: 5,
    })
    const filePath = "https://dweb.link/ipfs/" + cid
    return filePath
  } catch (e) {
    toasts.add({
      description: "file did not upload properly (" + e + ")",
      type: "error",
    })
  }
}

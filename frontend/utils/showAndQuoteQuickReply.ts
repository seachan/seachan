export function showAndQuoteQuickReply(postId) {
  var quickreply = document.getElementById("quickreply")
  quickreply.classList.remove("hidden")
  var textarea = document.getElementById(
    "quickreply-textarea",
  ) as HTMLInputElement
  if (textarea) {
    textarea.value += ">>" + postId + "\n"
    textarea.focus()
  }
}

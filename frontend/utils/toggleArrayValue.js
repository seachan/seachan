export function toggleArrayValue(arrayList, arrayValue) {
  return arrayList.includes(arrayValue)
    ? arrayList.filter((el) => el !== arrayValue)
    : [...arrayList, arrayValue]
}

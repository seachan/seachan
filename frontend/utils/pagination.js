import { POSTS_PER_PAGE } from "../constants"

export function pagination(posts, pageNumber, view) {
  if (view == "catalog" || view == "list") {
    return [1, posts]
  } else {
    let totalPages = Math.ceil(posts.length / POSTS_PER_PAGE)
    let indexOfLastRecord = pageNumber * POSTS_PER_PAGE
    let indexOfFirstRecord = indexOfLastRecord - POSTS_PER_PAGE
    let paginatedThreads = posts?.slice(indexOfFirstRecord, indexOfLastRecord)
    return [totalPages, paginatedThreads]
  }
}

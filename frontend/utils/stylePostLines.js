let quotePattern = /(>>[0-9]+)/ // >>0
import { highlightedPosts } from "../stores/postsStore"

export function stylePostLines(post) {
  post = post.body.replaceAll("&gt;", ">")
  let result = post
    .split("\n")
    .map((line) => {
      if (line[0] == ">" && line[1] != ">") {
        return '<span style="color: var(--theme-green)">' + line + "</span>"
      } else if (quotePattern.test(line)) {
        var quotedPostId = line.replace(">>", "")
        // highlightedPosts.update((p) => {
        //   p.push(post.boardId + "/" + quotedPostId)
        //   return p
        // })
        if (post.boardId + "/" + quotedPostId == post.threadId) {
          line = line.concat(" (OP)")
        }
        let color =
          '<span style="color: var(--theme-quote)">' + line + "</span>"

        let link =
          "<a href=#" +
          quotedPostId +
          ' style="text-decoration:none;">' +
          color +
          "</a>"

        let button =
          "<button class='fa' on:click={() => addHighlightedPost(" +
          quotedPostId +
          ")}>" +
          link +
          "</button>"
        return button
      }
      // no changes
      else {
        return line
      }
    })
    .join("\n")
  return result
}

function addHighlightedPost() {}

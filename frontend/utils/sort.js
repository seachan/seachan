export function sort(objs, sortBy) {
  let sortModifier = sortBy.ascending ? 1 : -1
  let sortField = sortBy.col.split(".")
  if (sortField.length == 1) {
    return objs.sort((a, b) =>
      a[sortBy.col] > b[sortBy.col]
        ? 1 * sortModifier
        : b[sortBy.col] > a[sortBy.col]
        ? -1 * sortModifier
        : 0,
    )
  } else if (sortField.length == 2) {
    return objs.sort((a, b) =>
      a[sortField[0]][sortField[1]] > b[sortField[0]][sortField[1]]
        ? 1 * sortModifier
        : b[sortField[0]][sortField[1]] > a[sortField[0]][sortField[1]]
        ? -1 * sortModifier
        : 0,
    )
  } else if (sortField.length == 3) {
    return objs.sort((a, b) =>
      a[sortField[0]][sortField[1]].length >
      b[sortField[0]][sortField[1]].length
        ? 1 * sortModifier
        : b[sortField[0]][sortField[1]].length >
          a[sortField[0]][sortField[1]].length
        ? -1 * sortModifier
        : 0,
    )
  }
}

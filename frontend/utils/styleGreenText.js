export function styleGreenText(body) {
  let greenTextPattern = /^>[^>]/ // >greentext
  let quotePattern = /(>>[0-9]+)/g // >>0
  let output = body
    .split("\n")
    .map((line) => {
      if (line.substring(0, 2) == ">>") {
        // add class
        return '<span style="color: var(--theme-green)">' + line + "</span>"
        //   return "<span className="green-text">{line}</span>
      } else {
        return ""
      }
      // else if (quotePattern.exec(line)) {
      // add class
      //   return wrapTags(
      //     line,
      //     quotePattern,
      //     "quoteLink",
      //     boardAbbreviation,
      //     threadKey,
      //     highlightedPosts,
      //     setHighlightedPosts,
      //   )
      // }
      // else {
      //   return ""
      // }
    })
    .join("\n")
  return output
}

export function getElapsedDays(timestamp) {
    let nanosecondNumber = Number(BigInt(timestamp))
    let seconds = nanosecondNumber / 1000000
    let dateObject = new Date(seconds)
    let todaysDate = new Date()
    let diff = todaysDate.getTime() - dateObject.getTime()
    return diff / (1000 * 60 * 60 * 24)
  }
import { get } from "svelte/store"
import type { Gate } from "src/declarations/backend/backend.did"
import { userNFTs, fetchUserNFTs, userStore } from "../stores/userStore"

export function isAllowedThroughNFTGate(gate: Gate, userNFTs) {
  let allowed = false
  if (gate && gate.token == "" && gate?.nft == "") {
    allowed = true
  }
  if (gate && gate.nft != "") {
    if (userNFTs) {
      var nft = gate.nft
      userNFTs.map((asset: { name: any }) => {
        if (asset.name == nft) {
          allowed = true
        }
      })
    }
  }
  return allowed
}

import { get } from "svelte/store"
import type { Gate } from "src/declarations/backend/backend.did"

export function isAllowedThroughTokenGate(gate: Gate, assets) {
  let allowed = false
  if (gate && gate.token == "") {
    allowed = true
  }
  if (gate && gate.token != "") {
    var token = gate.token
    var amount = gate.amount
    if (assets) {
      assets?.map((asset: { name: any; amount: number; symbol: any }) => {
        if (asset.symbol == token && asset.amount > amount) {
          allowed = true
        }
      })
    }
  }

  return allowed
}

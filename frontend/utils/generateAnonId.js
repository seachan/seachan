import { nanoid, customRandom, urlAlphabet, customAlphabet } from "nanoid"
import seedrandom from "seedrandom"
import { ID_LENGTH } from "../constants"

export function generateAnonId(seed = "") {
  let lower = "abcdefghijklmnopgrstuvwxyz"
  let upper = lower.toUpperCase()
  let nanoid = customAlphabet(lower + upper, ID_LENGTH)

  if (seed == "") {
    return nanoid()
  } else {
    const rng = seedrandom(seed)
    nanoid = customRandom(lower + upper, ID_LENGTH, (size) => {
      return new Uint8Array(size).map(() => 256 * rng())
    })
    return nanoid()
  }
}

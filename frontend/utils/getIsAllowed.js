import { isAllowedThroughTokenGate } from "./isAllowedThroughTokenGate"
import { isAllowedThroughNFTGate } from "./isAllowedThroughNFTGate"

export function getIsAllowed(gate, assets, userNFTs) {
    if (gate.token != "") {
      return isAllowedThroughTokenGate(gate, assets)
    }
    if (gate.nft != "") {
      return isAllowedThroughNFTGate(gate, userNFTs)
    } else {
      return true
    }
  }
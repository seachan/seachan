// needed for scrolling back up during navigation
history.pushState = new Proxy(history.pushState, {
  apply(target, thisArg, argumentsList) {
    Reflect.apply(target, thisArg, argumentsList)
    scrollTo(0, 0)
  },
})

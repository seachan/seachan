export function getCondensedPrincipal(principal) {
  return (
    principal.toString().split("-")[0] +
    "..." +
    principal.toString().split("-").at(-1)
  )
}

import { get, writable, derived } from "svelte/store"
// import { anonIdToThreadId } from "../utils/anonIdToThreadId"
import { highlightedPosts, threadRootStore } from "../stores/postsStore"
export function highlightAllPostsByPosterId(inPost) {
  highlightedPosts.set([])
  let posts = get(threadRootStore)
  let foundPosts = []
  highlightedPosts.set([])
  posts.forEach((post) => {
    if (
      JSON.stringify(post.poster.posterId) ===
      JSON.stringify(inPost.poster.posterId)
    ) {
      foundPosts.push(post.id)
    }
  })
  highlightedPosts.set(foundPosts)
}

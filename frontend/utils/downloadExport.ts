export function downloadExport(content, fileName) {
  var dataStr =
    "data:text/json;charset=utf-8," +
    encodeURIComponent(
      JSON.stringify(content, function (key, value) {
        if (
          typeof value === "bigint" ||
          // key === "ownerPrincipal" ||
          key === "principal"
        ) {
          return value.toString()
        } else if (key === "seaChadPrincipals" || key === "voters") {
          var convertedSeaChadPrincipals = []
          value.forEach((seaChadPrincipals) => {
            convertedSeaChadPrincipals.push(seaChadPrincipals.toString())
          })
          return convertedSeaChadPrincipals
        } else {
          return value
        }
      }),
    )
  var dlAnchorElem = document.getElementById("downloadAnchorElem")
  dlAnchorElem.setAttribute("href", dataStr)
  dlAnchorElem.setAttribute("download", fileName)
  dlAnchorElem.click()
}
